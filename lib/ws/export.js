/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
const Q = require('q');
const _ = require('lodash');
const express = require('express');
const mime = require('mime');
const invokerTool = require('../util/invoker');


const conf = require('../conf');
const wsUtil = require('./util');

const db = require('../api/datasource');
const auth = require('../api/auth');


const logger = require('../api/logger').get('sys.ws.export');


const controller = {};


const wsRoot = conf.urlBase+'/export';

const dbWs = require('./db');

/**
 * init()
**/
exports.init = function(app) {
  var router = express.Router();

  router.get('/:exportId/:className', wsUtil.wrap(controller.performExport));

  app.use(wsRoot, router);
}



var cleanupProjection = function(projectionObj) {
  var anyInclusion = false;
  var exclusionFields = [];

  for(var fieldName in projectionObj) {
    if(projectionObj[fieldName] === 0) {
      exclusionFields.push(fieldName);
    }
    else {
      anyInclusion = true;
    }
  }
  //If there's any inclusion fields, then all others are excluded by default
  if(anyInclusion) {
    for(var i=0; i < exclusionFields.length; i++) {
      delete projectionObj[exclusionFields[i]];
    }
  }
};


/**
 *  Takes a query, streams the results to the proper DataExport transformer
 *  The DataExport transformer reads the object stream, writes to the output stream
 **/
controller.performExport = function(req, res) {

  var className = req.params.className;
  var exportId = req.params.exportId;

  logger.info('performExport: %s, %s', className, exportId);

  if(!className || !exportId || !db[className]) {
    return wsUtil.handleError(res, 'Missing/invalid required parameter', 404);
  }

  var dbQuery = wsUtil.extractDbQuery(req);

  var TargetModel = db[className];

  var dataExportPromise = db.DataExport.findOne({_id:exportId});

  db.DataExport.findOne({_id:exportId}).then(dataExportObj=>{
    return dbWs.constructQuery(req, className, dbQuery.where, dbQuery.select, dbQuery.sort).then(qr=>{
      console.log('Constructed Query: %j', qr);
      const query = qr.queryPromise;

      query.lean && query.lean(true); //return plain objects instead of full-fledged mongoose docs

      var contentType = dataExportObj.content_type;
      var ext = '';

      if(contentType) {
        res.type(contentType);
        ext = mime.extension(contentType);
        ext = ext ? '.'+ext : '';
      }
      res.attachment(className+'_export'+ext);

      //Now invoke the DataExport.transform_fn
      logger.debug('Invoking DataExport %s', dataExportObj.name);


      var transformParams = {
        className: className,
        query:qr.queryObj,
        getInputStream:function() { return qr.pipeline ? query.cursor({batchSize:1000}).exec() : query.cursor() },
        outputStream:res,
        params:req.query
      }
      invokerTool.invokeInjected(dataExportObj.transform_fn, transformParams, dataExportObj);
    },
    function(err) {
      //AggregateReadDacs rejected promise indicates auth failure
      wsUtil.handleError(res, err, 401);
    });
  });

};
