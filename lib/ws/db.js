/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/*
db Web Service
  Defines the server-side logic for datasource api webservice endpoints
   * GET     /db/:className              ->  list
   * GET     /db/:className/:id          ->  get
   * POST    /db/:className              ->  save
   * DELETE  /db/:className/:id          ->  remove
*/
const Q = require('q');
const _ = require('lodash');
const express = require('express');

const conf = require('../conf');
const wsUtil = require('./util');

const db = require('../api/datasource');
const auth = require('../api/auth');

const logger = require('../api/logger').get('sys.ws.db');

const controller = {};


const wsRoot = conf.urlBase+'/db';

/**
 * init()
**/
exports.init = function(app) {
  const router = express.Router();

  router.get('/:className', wsUtil.wrap(controller.list));
  router.get('/:className/:id', wsUtil.wrap(controller.get));

  router.post('/:className', wsUtil.wrap(controller.save));
  router.post('/:className/:id', wsUtil.wrap(controller.save));

  router.delete('/:className', wsUtil.wrap(controller.remove));
  router.delete('/:className/:id', wsUtil.wrap(controller.remove));

  app.use(wsRoot, router);
}


/**
 * utility function to strip fields from modelObj that are marked as excluded in projectionObj
**/
var stripForbiddenFields = function(projectionObj, modelObj) {
  var anyChange = false;
  for(var fieldName in projectionObj) {
    if(projectionObj[fieldName] == 0 && modelObj.hasOwnProperty(fieldName)) {
      anyChange = true;
      delete modelObj[fieldName];
    }
  }
  return anyChange;
}

/**
 * utility function to clean up projection so it doesn't have a mixture of inclusion/exclusion
**/
var cleanupProjection = function(projectionObj) {
  var anyInclusion = false;
  var exclusionFields = [];

  for(var fieldName in projectionObj) {
    if(projectionObj[fieldName] === 0) {
      exclusionFields.push(fieldName);
    }
    else {
      anyInclusion = true;
    }
  }
  //If there's any inclusion fields, then all others are excluded by default
  if(anyInclusion) {
    for(var i=0; i < exclusionFields.length; i++) {
      delete projectionObj[exclusionFields[i]];
    }
  }
};



const getRefClassName = function(td) {
  let refModelMd = db[td.ref_class]._bo_meta_data;
  if(refModelMd.super_model) {
    refModelMd = refModelMd.super_model._bo_meta_data;
  }

  return refModelMd && refModelMd.class_name;
}

/**
 * Examine references in the projection; summarize any reference fields that are requested. 
 * 
 * (only supports single-step, i.e. no ref1.ref2.field)
 * @return object mapping field name to summary object: {className, base, denorm, ext}
 *   denorm, ext: array of {field, fq}
 */
const summarizeProjection = function(TargetModel, projectionObj) {

  const tdm = TargetModel._bo_meta_data.type_desc_map;

  const result = {}; //field_name -> {base, denorm, ext}

  for(var fieldSpec in projectionObj) {

    let dotPos = fieldSpec.indexOf('.');

    if(dotPos < 0) {
      let td = tdm[fieldSpec];
      if(td && td.type === 'reference') {
        let r = result[fieldSpec] = result[fieldSpec] || {};
        r.base = true;
        r.className = getRefClassName(td);
      }
    }
    else {
      let localField = fieldSpec.substring(0, dotPos);
      let refField = fieldSpec.substring(dotPos+1);
      let td = tdm[localField];

      if(td && td.type === 'reference') {
        let r = result[localField] = result[localField] || {};
        r.className = r.className || getRefClassName(td);
        
        if(td.denormalize_fields && td.denormalize_fields.indexOf(refField)>-1) {
          //It's asking for a field into a reference, but it is denormalized.
          result.denormsRequired = true;
          r.denorm = r.denorm || [];
          r.denorm.push({field:refField, fq:fieldSpec});
        }
        else {
          //It's asking for a field into a reference that isn't denormalized 
          //  (will require aggregation pipeline to obtain)
          result.pipelineRequired = true;
          r.ext = r.ext || [];
          r.ext.push({field:refField, fq:fieldSpec});
        }
      }

    } //end dotpos check

  }//end projectionObj loop

  return result;

};



//TODO: will this fail when sort-by digs into a non-denormalized reference field?
const massageSortExpression = function(sortExpr, tdm) {
  if(!sortExpr) {
    return;
  }

  _.forEach(sortExpr, (val, field) =>{
    var td = tdm[field];
    if(td instanceof Array) {
      td = td[0];
    }

    if(td && td.type==='reference') {
      sortExpr[field+'._disp'] = val;
      delete sortExpr[field];
    }
  });
}

/**
 * Construct query promise, taking into account current user and DACs
 * @return {queryObj, projection, queryPromise}
 */
const constructQuery = 
exports.constructQuery = function(req, className, conditions, proj, sort, limit, skip, groupBy) {


  if(limit !== undefined)
    limit = +limit;

  if(skip !== undefined)
    skip = +skip;

  var TargetModel = db[className];
  massageSortExpression(sort, TargetModel._bo_meta_data.type_desc_map);

  return Q.all([
    auth.getCurrentUser(req),
    auth.aggregateReadDacs(req, TargetModel)
  ])
  .spread(function(currUser, dacObj){

    var dacCond = dacObj.condition;
    var dacProj = dacObj.fieldRestrictions;

    //Incorporate DAC restrictions into the requested query
    var queryObj;
    if(dacCond) {
      if(conditions)
        queryObj = {$and:[conditions, dacCond]};
      else
        queryObj = dacCond;
    }
    else {
      queryObj = conditions || {};
    }
    
    queryObj.$useContext = {currentUser:currUser};
    
    logger.debug("Constructing query for %s: %j", className, queryObj);

    //Incorporate field restrictions into the projection
    if(dacProj) {
      if(proj)
        _.assign(proj, dacProj);
      else
        proj=dacProj;
    }


    cleanupProjection(proj);

    
    const projectionSummary = summarizeProjection(TargetModel, proj);

    var query;

    if(!groupBy && !projectionSummary.pipelineRequired) {
      //Simple case: not a group-by query.  just do a find.
      logger.debug('%s.find(%j, %j)', className, queryObj, proj);
      logger.silly('PROJECTION SUMMARY: %j', projectionSummary);

      if(projectionSummary.denormsRequired) {
        _.forEach(projectionSummary, (fieldDesc, localField)=>{
          if(fieldDesc.denorm) {
            if(fieldDesc.base) {
              //Asking for the base field is enough; denorms will come automatically
              fieldDesc.denorm.forEach(d=>{
                delete proj[d.fq];
              });
            }
            else {
              //make sure the _id comes through
              let hasId = false;
              fieldDesc.denorm.forEach(d=>{
                hasId = hasId || (d.field === '_id');
              });
              if(!hasId) {
                proj[localField+'._id'] = 1;
              }
            }
          }
        });
      }


      query = TargetModel.find(queryObj, proj);

      sort && query.sort(sort);
      skip && query.skip(skip);
      limit && query.limit(limit);

      return {
        queryObj,
        projection:proj,
        queryPromise:query
      };

    }
    else if(projectionSummary.pipelineRequired) {
      //Build pipeline to $lookup referenced fields

      //FIXME: 
      query = TargetModel.aggregate()
        .match(queryObj);

      if(sort && Object.keys(sort).length)
        query.sort(sort);
      if(skip)
        query.skip(skip);
      if(limit)
        query.limit(limit);

      //Special option to trigger post-processing hook
      query.options._noonianClass = className;


      const temporaryFields = []; //for nested dereferenced documents

      _.forEach(projectionSummary, (fieldDesc, localField)=>{
        //fieldDesc: {className, base, denorm, ext}
        if(!fieldDesc.ext) {
          return; //No special processing needed if there's no external fields asked for
        }

        //When we pull in the referenced object into the pipeline,
        //  it will go into local_field._deref
        let derefContainerField = localField+'._deref';
        temporaryFields.push(derefContainerField);

        //Add the "join" to the referenced collection
        query.lookup({
            from:fieldDesc.className, 
            localField:localField+'._id', 
            foreignField:'_id', 
            as:derefContainerField
        })
        .unwind(derefContainerField);

        fieldDesc.ext.forEach(ext=>{
          //Add a reshaping projection to move 
          //doc.localField._deref.refField ->  doc.localField.refField
          let sourceField = derefContainerField+'.'+ext.field;
          let targetField = localField+'.'+ext.field;

          proj[targetField] = '$'+sourceField;
        });

        if(fieldDesc.base) {
          //Cant have the projection ask for ref_field AND ref_field.subfield
          delete proj[localField];
        }

        proj[localField+'._id'] = '$'+derefContainerField+'._id';
        proj[localField+'._disp'] = '$'+derefContainerField+'.__disp';

      }); //end _.forEach(projectionSummary)

      

      //Pull in all requested fields, and pull in the ref fields from the nested dereferenced objects
      query.project(proj);

      //Remove any nested documents after their fields have been pulled out
      temporaryFields.forEach(f=>{
        query.project({[f]:0});
      });

      
      return {
        queryObj,
        projection:proj,
        queryPromise:query,
        pipeline:true
      };
    }
    else if(groupBy) {
      //Construct an aggretation pipeline for groupBy...

      //sort by the groupBy field, to ensure consistent behavior for pagination
      // (don't want members of a group scattered accross different pages!)
      var gbSort = groupBy;

      if(sort) {
        //If a sort was requested, make sure we do group-by sort *before* the requested sort
        for(var sf in sort) {
          if(sf == groupBy) continue;
          if(sort[sf] == -1 || sort[sf] === 'desc' || sort[sf] === 'descending')
            gbSort += ' -'+sf;
          else
            gbSort += ' '+sf;
        }
      }

      if(TargetModel._bo_meta_data.getTypeDescriptor(groupBy).type === 'reference') {
        //TODO, use FieldTypeService to extract groupBy and sort clauses
        groupBy = groupBy+'._id';
      }

      query = TargetModel.aggregate()
        .match(queryObj);

      //https://jira.mongodb.org/browse/SERVER-13715
      if(proj)
        query.project(proj);

      query.sort(gbSort);


      if(skip)
        query.skip(skip);
      if(limit)
        query.limit(limit);

      query.group({
        _id:'$'+groupBy,
        group:{$push:'$$CURRENT'}
      })
      .sort({_id:1});


      var countQuery = TargetModel.aggregate()
        .match(queryObj)
        .group({
          _id:'$'+groupBy,
          count:{$sum:1}
        });


      const queryPromise = Q.all([query.exec(), countQuery.exec()]).spread(function(results, counts) {
        
        var countGroups = _.indexBy(counts, '_id'); //Key's the elements in resultArr[1] by _id property

        _.forEach(results, function(groupObj) {
          var countObj = countGroups[groupObj._id];
          if(countObj) {
            groupObj.count = countObj.count;
          }
        });

        return results;
      });


      return {
        queryObj,
        projection:proj,
        queryPromise,
        pipeline:true
      };
    }


  }); //end Q.all(auth stuff)
  

};




controller.list = function(req, res) {

  var className = req.params.className;

  var conditions = null,
    fields,
    sort,
    groupBy = req.query.groupBy,
    limit = req.query.limit,
    skip = req.query.skip;

  if(limit !== undefined)
    limit = +limit;

  if(skip !== undefined)
    skip = +skip;

  if(req.query.where) {
    try {
      conditions = JSON.parse(req.query.where);
    } catch(e) {logger.error(e)}
  }
  if(req.query.select) {
    try {
      fields = JSON.parse(req.query.select);
    } catch(e) {logger.error(e)}
  }
  if(req.query.sort) {
    try {
      sort = JSON.parse(req.query.sort);
    } catch(e) {logger.error(e)}
  }

  logger.silly('LIST: %s %j %j %j %s %s', className,conditions,fields,sort,limit,skip);

  constructQuery(req, className, conditions, fields, sort, limit, skip, groupBy).then(
      function(query) {
        return  Q.all([
          db[className].count(query.queryObj),
          query.queryPromise
        ])
        .spread((totalRecords, result) =>{
          res.json({
            result,
            nMatched:totalRecords,
            queryObj:query.queryObj
          })
        })

      },
      wsUtil.handleError.bind(this, res)
  );

};



// Get a single BO
controller.get = function(req, res) {
  var className = req.params.className;
  var id = req.params.id;

  var fields;
  if(req.query.select) {
    try {
      fields = JSON.parse(req.query.select);
    } catch(e) {logger.error(e)}
  }


  var TargetModel = db[className];

  //auth.aggregateReadDacs(req, TargetModel).then(function(dacObj){
  Q.all([
    auth.getCurrentUser(req),
    auth.aggregateReadDacs(req, TargetModel)
  ])
  .then(function(resultArr){
    var currUser = resultArr[0].toPlainObject()
    var dacObj = resultArr[1];
      
    var dacCond = dacObj.condition;
    var dacProj = dacObj.fieldRestrictions;

    var queryObj = {_id:id};

    if(dacCond) {
      queryObj = {$and:[queryObj, dacCond]};
      queryObj.$useContext = {currentUser:currUser};
    }

    if(dacProj) {
      if(fields)
        _.assign(fields, dacProj);
      else
        fields=dacProj;
    }
    cleanupProjection(fields);
    
    
    TargetModel.findOne(queryObj, fields, function(err, result){
      if(err) { return wsUtil.handleError(res, err); }
      if(!result) { return wsUtil.handleError(res, className+" "+id+" not found", 404); }
      return res.json({result:result});
    });

  },
  function(err) {
    //AggregateReadDacs rejected promise indicates auth failure
    wsUtil.handleError(res, err, 401);
  });

};



//Save: insert or update a single item, or batch update multiple according to criteria
controller.save = function(req, res) {
  var className = req.params.className;
  var id = req.params.id;
  if(req.body._id) {
    if(!id)
      id = req.body._id;
    delete req.body._id;
  }

  var conditions;
  if(req.query.where) {
    conditions = JSON.parse(req.query.where); //May throw exception
  }

  var TargetModel = db[className];

  if(id) {
    /*
      *** Single-item update ***
    */
    //auth.aggregateUpdateDacs(req, TargetModel).then(function(dacObj){
    Q.all([
        auth.getCurrentUser(req),
        auth.aggregateUpdateDacs(req, TargetModel)
    ])
    .then(function(resultArr){
      var currUser = resultArr[0].toPlainObject();
      var dacObj = resultArr[1];
    
      var dacCond = dacObj.condition;
      var dacProj = dacObj.fieldRestrictions;

      //DAC Update requirments:
      // 1. the existing record conforms to the "condition" specified by the DAC
      // 2. no fields are updated that "field restrictions" forbid
      var queryObj = {_id:id};

      //Append the DAC condition to the query
      if(dacCond) {
        queryObj = {$and:[queryObj, dacCond]};
        queryObj.$useContext = {currentUser:currUser};
      }
      
      TargetModel.findOne(queryObj, null, function(err, result) {
        if(err) { return wsUtil.handleError(res, err); }
        if(!result) { return wsUtil.handleError(res, "Not authorized to update", 401); }

        //We've got the existing record, which passes the DAC conditions.

        //Now, just strip out any forbidden fields, apply changes to the existing record, and commit the update.
        var newObj = req.body;
        if(dacProj)
          if(stripForbiddenFields(dacProj, newObj)) {
            logger.warn('Attempted to update restricted field: user %s, record %s %s', req.user._id, className, id);
          }

        _.assign(result, newObj); //Apply fields from newObj atop result
        
        return result.save({currentUser:currUser}, null).then(function (saveResult) {
          delete saveResult._current_user;
          return res.json({result:saveResult, nModified:1});
        },
        function(err) {
          return wsUtil.handleError(res, err);
        });

      });

    },
    function(err) {
      //AggregateXyzDacs rejected promise indicates auth failure
      wsUtil.handleError(res, err, 401);
    });
  }
  else if(conditions) {
    /*
      *** Batch update ***
    */
    //auth.aggregateUpdateDacs(req, TargetModel).then(function(dacObj){
    Q.all([
        auth.getCurrentUser(req),
        auth.aggregateUpdateDacs(req, TargetModel)
    ])
    .then(function(resultArr){
      var currUser = resultArr[0].toPlainObject()
      var dacObj = resultArr[1];
      
      var dacCond = dacObj.condition;
      var dacProj = dacObj.fieldRestrictions;

      //Incorporate DAC restrictions into the requested query
      var queryObj;
      if(dacCond) {
        queryObj = {$and:[conditions, dacCond]};
      }
      else {
        queryObj = conditions;
      }

      var updateObj = req.body;
      if(dacProj)
        if(stripForbiddenFields(dacProj, updateObj))
          logger.warn('Attempted to update restricted field: user %s, record %s %s', req.user._id, className, id);
    
      
      
      //TODO: mongoose batch update is not yet wrapped; therefore no data triggers or noonianContext
      
      TargetModel.update(queryObj, updateObj, {multi:true}, function(err, result) {
        if(err)
          return wsUtil.handleError(res, err);
        result.result="success";
        return res.json(result);
      });

    },
    function(err) {
      //AggregateXyzDacs rejected promise indicates auth failure
      wsUtil.handleError(res, err, 401);
    });

  }
  else {
    /*
      *** Single insert ***
    */
    Q.all([
        auth.getCurrentUser(req),
        auth.aggregateCreateDacs(req, TargetModel)
    ])
    .then(function(resultArr){
      var currUser = resultArr[0].toPlainObject()
      var dacObj = resultArr[1];

      var dacCond = dacObj.condition;
      var dacProj = dacObj.fieldRestrictions;

      var newObj = req.body;
      if(dacProj)
        if(stripForbiddenFields(dacProj, newObj))
          logger.warn('Attempted to insert w/ restricted field: user %s, record %s %j', req.user._id, className, newObj);

      var newModelObj = new TargetModel(newObj);

      if(dacCond) {
          db._svc.QueryOpService.applyNoonianContext(dacCond, {currentUser:currUser});
      }

      if(auth.checkCondition(dacCond, newModelObj)) {
        
        return newModelObj.save({currentUser:currUser}, null).then(function(saveResult) {
          
          delete saveResult._current_user;
          //Respond with the inserted object as the result
          return res.json({result:saveResult, nInserted:1});
        },
        wsUtil.handleError.bind(null, res)
        );
      }
      else {
        wsUtil.handleError(res, 'Not authorized to insert', 401);
      }

    },
    function(err) {
      //AggregateXyzDacs rejected promise indicates auth failure
      wsUtil.handleError(res, err, 401);
    });
  }
};



//Remove: either a single by ID or batch based on criteria.
controller.remove = function(req, res) {
  var className = req.params.className;
  var id = req.params.id;
  var conditions;

  if(req.query.where) {
    conditions = JSON.parse(req.query.where);
  }

  var TargetModel = db[className];

  if(id) {
    conditions = {_id:id};
  }
  else if(!conditions) {
    return wsUtil.handleError(res, "No conditions specified");
  }


  Q.all([
    auth.getCurrentUser(req),
    auth.aggregateDeleteDacs(req, TargetModel)
  ])
  .then(function(resultArr) {
    var currUser = resultArr[0];
    var dacObj = resultArr[1];
    var dacCond = dacObj.condition;

    var queryObj;
    if(dacCond) {
      queryObj = {$and:[conditions, dacCond]};
    }
    else {
      queryObj = conditions;
    }
    
    queryObj.$useContext = {currentUser:currUser};

    TargetModel.remove(queryObj, function(err, result) {
      if(err) { return wsUtil.handleError(res, err); }
      return res.json({result:"success", nRemoved:result.length}); //TODO if result.length=0, is it success?
    });

  },
  function(err) {
    //AggregateXyzDacs rejected promise indicates auth failure
    wsUtil.handleError(res, err, 401);
  });

};
