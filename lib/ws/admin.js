/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
const Q = require('q');
const _ = require('lodash');
const express = require('express');

const conf = require('../conf');
const wsUtil = require('./util');

const db = require('../api/datasource');
const auth = require('../api/auth');

const logger = require('../api/logger').get('sys.ws.admin');

var controller = {};


var wsRoot = conf.urlBase+'/admin';

/**
 * init()
**/
exports.init = function(app) {
  var router = express.Router();

  router.get('/performRepair', wsUtil.wrap(controller.performRepair));

  app.use(wsRoot, router);
}


controller.performRepair = function(req, res) {
  logger.info('db/performRepair ws called');
  wsUtil.sendPromiseResult(res, db._svc.RefService.repair());
};
