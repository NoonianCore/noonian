/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 *Package Web Service
 */
'use strict';

const Q = require('q');
const _ = require('lodash');
const express = require('express');

const conf = require('../conf');
const wsUtil = require('./util');


const db = require('../api/datasource');
const GridFsService = require('../api/datasource/gridfs');
const PackagingService = require('../api/datasource/packaging');


// const logger = require('../api/logger').get('sys.ws.package');

const controller = {};


const wsRoot = conf.urlBase+'/pkg';

/**
 * init()
**/
exports.init = function(app) {
  var router = express.Router();

  router.get('/createUpgrade', wsUtil.wrap(controller.createUpgrade));
  router.get('/applyPackage', wsUtil.wrap(controller.applyPackage));

  app.use(wsRoot, router);
}



/**
 *  Run against a BusinessObjectPackage record
**/
controller.createUpgrade = function(req, res) {
  var bopId = req.query.id;

  PackagingService.buildPackage(bopId).then(function(result) {
    res.json({message:'created package '+result});
  },

  wsUtil.handleError.bind(null, res)
  );
};

controller.applyPackage = function(req, res) {
  var bopId = req.query.id;

  PackagingService.applyPackage(bopId).then(function(result) {
    res.json({message:'applied package '+result});
  },

  wsUtil.handleError.bind(null, res)
  );
};


