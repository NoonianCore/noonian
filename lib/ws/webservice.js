/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';
const Q = require('q');
const _ = require('lodash');
const express = require('express');

const conf = require('../conf');
const wsUtil = require('./util');

const db = require('../api/datasource');
const auth = require('../api/auth');


// const logger = require('../api/logger').get('sys.ws.webservice');

const invokerTool = require('../util/invoker');


const controller = {};
const wsRoot = conf.urlBase+'/ws';

/**
 * init()
**/
exports.init = function(app) {
  const router = express.Router();

  router.all('/*', wsUtil.wrap(controller.invokeWebservice));

  app.use(wsRoot, router);
}


controller.invokeWebservice = function(req, res) {

  db.WebService.findOne({path:req.path}).exec().then(function(wsObj){

    if(!wsObj || !wsObj['function']) {
      return wsUtil.handleError(res, req.path+' not found', 404);
    }


    //Check permissions...
    auth.checkRoles(req, wsObj.rolespec).then(
      function() {
        var injectables = {
          req:req,
          res:res,
          queryParams:req.query,
          postBody:req.body
        };

        var toCall = wsObj['function'];
        invokerTool.invokeAndReturnPromise(toCall, injectables, wsObj).then(
          function(retVal) {
            if(retVal && retVal.__stream_response) {
              retVal.__stream_response.pipe(res);
            }
            else {
              if(!res.get('Content-Type')) {
                res.json(retVal);
              }
              else {
                res.send(retVal);
              }
              
            }
          },
          function(err) {
            wsUtil.handleError(res, err);
          }
        );

      },
      function(err) {
        //Role check failed... send 401 status
        wsUtil.handleError(res, err, 401);
      }
    );

  });

};

