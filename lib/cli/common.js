const fs = require('fs');
const path = require('path');
const os = require('os');

//this module's __dirname should be NOONIAN_HOME/lib/cli
const NOONIAN_HOME = exports.NOONIAN_HOME = path.resolve(__dirname, '..', '..');

//NOONIAN_USER_HOME = ~/.noonian
const NOONIAN_USER_HOME = exports.NOONIAN_USER_HOME = path.resolve(os.homedir(), '.noonian');


const instanceIndexFile = process.env.NOONIAN_INSTANCE_INDEX || path.resolve(NOONIAN_USER_HOME, 'instance-index.json');
var instances;
var instanceFileContents;

try {
	instanceFileContents = fs.readFileSync(instanceIndexFile);
}
catch(err) {}

try {
	if(instanceFileContents) {
		instances = exports.instanceIndex = JSON.parse(instanceFileContents);
	}
	else {
		instances = exports.instanceIndex = {};
	}
}
catch(err) {
	console.error('problem parsing instance index');
	exports.instanceIndex = {};
}


const saveInstanceIndex = 
exports.saveInstanceIndex = function() {
	try {
		fs.statSync(NOONIAN_USER_HOME);
	}
	catch(err) {
		if(err.code !== 'ENOENT') {
			console.log(err);
		}
		fs.mkdirSync(NOONIAN_USER_HOME);
	}

	fs.writeFileSync(instanceIndexFile, JSON.stringify(instances, null, 2));

	let instanceListFile = path.resolve(NOONIAN_USER_HOME, 'instance-list');
	fs.writeFileSync(instanceListFile, Object.keys(instances).join(' '));
};

const getInstanceConf = 
exports.getInstanceConf = function(instanceDir) {
	try {
		const instanceConf = require(path.resolve(instanceDir, 'instance-config.js'));
		return instanceConf;
	}
	catch(err) {}
	return null;
};

const getInstanceName =
exports.getInstanceName = function(instanceDir) {
	const instanceConf = getInstanceConf(instanceDir);
	return instanceConf && instanceConf.instanceName;
};

exports.checkValidInstanceSpec = function(name, cmd) {
	if(name && cmd.directory) {
		console.error('Please specify EITHER instance name OR instance directory, not both'.red);
		return false;
	}
	return true;
}

exports.getInstanceDir = function(name, cmd) {

	if(name && cmd.directory) {
		console.error('Please specify EITHER instance name OR instance directory, not both'.red);
		return null;
	}
	if(!name && !cmd.directory) {
		let cwd = path.resolve();
		let nameFromDir = getInstanceName(cwd);
		if(!nameFromDir) {
			console.error('Invalid instance directory at %s\n try running "noonian init <instanceName>"'.red, cwd);
			return null;
		}
		return cwd;
	}

	
	if(name) {
		if(!instances[name]) {
			console.error('Instance "%s" not found in index. Try launching with --directory option'.red, name);
			return null;
		}
		let nameFromDir = getInstanceName(instances[name]);
		if(!nameFromDir) {
			console.error('Invalid instance directory at %s\n If you moved the instance directory, Try launching with the --directory option'.red, instances[name]);
			return null;
		}
		return instances[name];
	}

	//cmd.directory was specified
	const specifiedDir = path.resolve(cmd.directory);
	const nameFromDir = getInstanceName(specifiedDir);


	if(!nameFromDir) {
		console.error('Invalid instance directory at %s\n  missing or invalid instance-config.js (did you initialize the instance?)'.red, specifiedDir);
		return null;
	}
	

	if(!instances[nameFromDir]) {
		console.log('Creating entry in index for %s', nameFromDir);
		console.log('  (from now on you can launch with the command: "noonian start %s")', nameFromDir);
		instances[nameFromDir] = specifiedDir;
		saveInstanceIndex();
	}
	else if(instances[nameFromDir] !== specifiedDir) {
		console.log('Replacing index entry for %s (formerly located at %s)', nameFromDir, instances[nameFromDir]);

		instances[nameFromDir] = specifiedDir;
		saveInstanceIndex();
	}
	
	return specifiedDir;
	
}


exports.getAllConfigs = function() {
	const result = {};

	Object.keys(instances).forEach(n=>{
		let dir = instances[n];
		result[n] = getInstanceConf(dir);
	});
	return result;
};