const fs = require('fs');
const path = require('path');

const ps = require('ps-node');

const common = require('./common');


const getInstanceDir = common.getInstanceDir;
const getInstanceConf = common.getInstanceConf;
const instances = common.instanceIndex;


exports.status = function() {
	
	const names = Object.keys(instances);
	const pidMap = {};
	const nodeProcs = {};
	names.sort();
	names.forEach(n=>{
		let instanceDir = instances[n];
		try {
			let myPid = fs.readFileSync(path.resolve(instanceDir, 'pid'));
			pidMap[n] = myPid;
		}
		catch(err) {}
	});

	ps.lookup({command:'node'}, function(err, resultList) {
		if(err) {
			console.error('Failed to query running processes'.red);
			console.error(err);
			process.exit(1);
		}
		resultList.forEach(p=>{
			nodeProcs[p.pid] = true;
		});

		console.log('Instance Name  \tStatus    \tPID');
		console.log('-------------  \t------    \t---')

		names.forEach(n=>{
			let pid = pidMap[n] || '-';
			let status = nodeProcs[pid] ? 'RUNNING' : (pidMap[n] && 'DIED' || 'not running');

			if(nodeProcs[pid]) {
				status = 'RUNNING'.padEnd(11).green;
				n = n.padEnd(15).green;
			}
			else if(pidMap[n]) {
				//not running, but left the pid file hanging
				status = 'DIED'.padEnd(11).red;
				n = n.padEnd(15).red;
			}
			else {
				status = 'not running'.padEnd(11);
				n = n.padEnd(15);
			}

			console.log('%s\t%s\t%s', n, status, pid);
		});
		process.exit(0);
	});
};



exports.list = function() {
	const names = Object.keys(instances);

	console.log('Instance Name \tListen Port \tDatabase             \tDirectory');
	console.log('------------- \t----------- \t-------------------- \t---------');

	const dbRegex = /mongodb:\/\/[^\/]+\/([^\/?]+)/;

	names.sort();
	names.forEach(n=>{
		let dir = instances[n];
		let conf = getInstanceConf(dir);
		let db = dbRegex.exec(conf.mongo.uri);
		if(db) {
			db = db[1];
		}
		else {
			db = '*bad config*';
		}
		console.log('%s\t%s\t%s\t%s', n.padEnd(15), (''+conf.serverListen.port).padEnd(11), db.padEnd(20), dir);
	});
	process.exit(0);
};
