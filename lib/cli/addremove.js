const path = require('path');

const common = require('./common');

const instances = common.instanceIndex;
const getInstanceName = common.getInstanceName;
const saveInstanceIndex = common.saveInstanceIndex;


exports.add = function(cmd) {
	const instanceDir = path.resolve(cmd.directory || '.');
	const name = getInstanceName(instanceDir);
	if(!name) {
		console.error('Couldnt find a valid instance config in %s'.red, instanceDir);
		process.exit(1);
	}

	const existing = instances[name];
	if(existing) {
		if(existing === instanceDir) {
			console.log('Instance %s already exists in index', name);
			process.exit(0);
		}
		else {
			console.error('Instance %s already exists: %s'.red, name, existing);
			console.error(' (try running "noonian remove %s" first)'.red, name);
			process.exit(1);
		}
	}

	instances[name] = instanceDir;
	saveInstanceIndex();
	console.log('Successfully added %s as "%s"', instanceDir, name);
	process.exit(0);

};

exports.remove = function(name) {
	if(instances[name]) {
		delete instances[name];
		saveInstanceIndex();
		console.log('Removed instance "%s" from index', name);
	}
	else {
		console.log('Instance "%s" not found', name);
	}
};
