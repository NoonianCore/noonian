const path = require('path');
const {spawnSync} = require('child_process');

const Tail = require('tail').Tail;

const common = require('./common');

const getInstanceDir = common.getInstanceDir;
const getInstanceConf = common.getInstanceConf;

exports.watchConsole = function(name, cmd, _instanceDir) {
	const instanceDir = _instanceDir || getInstanceDir(name, cmd);
	const stdout = new Tail(path.resolve(instanceDir, 'stdout.log'));
	const stderr = new Tail(path.resolve(instanceDir, 'stderr.log'));

	stdout.on('line', line=>{
		console.log(line);
	});
	stderr.on('line', line=>{
		console.error(line.red);
	});

	const onErr = err=> {
		console.error(err);
	}
};


exports.launchBrowser = function(name, cmd, _instanceDir, noExit) {
	//xdg-open
	const instanceDir = _instanceDir || getInstanceDir(name, cmd);
	const conf = getInstanceConf(instanceDir);

	if(!conf || !conf.serverListen) {
		console.error('Couldnt find a valid instance config in %s'.red, instanceDir);
		return (!noExit) && process.exit(1);
	}

	let prefix = conf.useHttps ? 'https://' : 'http://';
	let launchUrl = prefix+conf.serverListen.host+':'+conf.serverListen.port;
	if(conf.urlBase) {
		launchUrl += '/'+conf.urlBase;
	}

	const execMap = {
		linux:'xdg-open',
		win32:'start',
		darwin:'open'
	};
	const myOpen = execMap[process.platform];

	if(!myOpen) {
		console.error('Couldnt figure out how to open based on platform "%s" (file an issue report!)'.red, process.platform);
		return (!noExit) && process.exit(1);
	}

	try {
		const spawnOpts = {
			stdio: 'inherit'
		};

        const callResult = spawnSync(myOpen, [launchUrl], spawnOpts);

        if(callResult.status !== 0) {
        	console.error('%s returned error status: %s'.red, myOpen, callResult.status);
        	return (!noExit) && process.exit(1);
        }
        return (!noExit) && process.exit(0);
        
    }
    catch(err) {
    	console.error('Problem spawning "%s"'.red, myOpen);
    	console.error(err);
		return (!noExit) && process.exit(1);
    }


};