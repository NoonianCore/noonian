const fs = require('fs');
const path = require('path');
const os = require('os');
const {fork} = require('child_process');

const ps = require('ps-node');

const common = require('./common');
const util = require('./util');

const getInstanceDir = common.getInstanceDir;
const NOONIAN_HOME = common.NOONIAN_HOME;

exports.getEnv = function(name, cmd, _instanceDir) {
	const instanceDir = _instanceDir || getInstanceDir(name, cmd);
	if(instanceDir) {
		console.log('# Environment variables for %s', instanceDir);

		const showLine = (name, val) => console.log(`export ${name}="${val}"`);

		showLine('NODE_PATH', path.resolve(instanceDir, 'node_modules'));
		showLine('NOONIAN_HOME', NOONIAN_HOME);
		showLine('NOONIAN_INSTANCE', instanceDir);
	}
	else {
		console.log('echo Invalid instance');
	}
}

const launchInstance = function(instanceDir) {

	console.log('STARTING %s', instanceDir);

	const stdout = fs.openSync(path.resolve(instanceDir, 'stdout.log'), 'a');
	const stderr = fs.openSync(path.resolve(instanceDir, 'stderr.log'), 'a');

	const env = process.env;
	env.NODE_PATH = path.resolve(instanceDir, 'node_modules');
	env.NOONIAN_HOME = NOONIAN_HOME;
	env.NOONIAN_INSTANCE = instanceDir;

	const childProc = fork(
		path.resolve(NOONIAN_HOME, 'lib/server.js'), 
		['--instanceDir', instanceDir], 
		{
			cwd:NOONIAN_HOME,
			detached:true,
			stdio:['ignore', stdout, stderr, 'ipc'],
			env:env
		}
	);

	childProc.unref(); //Allow this process to exit while the child runs
};

const start = 
exports.start = function(name, cmd, _instanceDir) {
	
	const instanceDir = _instanceDir || getInstanceDir(name, cmd);
	if(instanceDir) {
		console.log('STARTING %s', instanceDir);

		const stdout = fs.openSync(path.resolve(instanceDir, 'stdout.log'), 'a');
		const stderr = fs.openSync(path.resolve(instanceDir, 'stderr.log'), 'a');

		const env = process.env;
		env.NODE_PATH = path.resolve(instanceDir, 'node_modules');
		env.NOONIAN_HOME = NOONIAN_HOME;
		env.NOONIAN_INSTANCE = instanceDir;

		const childProc = fork(
			path.resolve(NOONIAN_HOME, 'lib/server.js'), 
			['--instanceDir', instanceDir], 
			{
				cwd:NOONIAN_HOME,
				detached:true,
				stdio:['ignore', stdout, stderr, 'ipc'],
				env:env
			}
		);

		childProc.unref(); //Allow this process to exit while the child runs

		if(cmd.open) {
			util.launchBrowser(null, null, instanceDir, true);
		}

		if(cmd.watch) {
			setTimeout(util.watchConsole.bind(null, null, null, instanceDir), 1000);
		}
		else {
			process.exit(0);
		}
	}

};

const startall = 
exports.startall = function() {
	const instances = common.instanceIndex;
	const names = Object.keys(instances);

	if(!names.length) {
		console.log('No instances configured');
		process.exit(0);
	}
	
	names.sort();
	names.forEach(n=>launchInstance(instances[n]));
	process.exit(0);
};

const restartOrExit = function(cmd, restart, exitCode, instanceDir) {
	if(restart) {
		return start(null, cmd, instanceDir);
	}
	else {
		process.exit(exitCode);
	}
};

const stop = 
exports.stop = function(name, cmd, doRestart) {
	const instanceDir = getInstanceDir(name, cmd);
	if(instanceDir) {
		console.log('STOPPING %s', instanceDir);

		var myPid
		try {
			myPid = fs.readFileSync(path.resolve(instanceDir, 'pid'));
		}
		catch(err) {
			console.error('Failed to read pid file for instance %s (are you sure it is running?)'.red, instanceDir);
			return restartOrExit(cmd, doRestart, 1, instanceDir);
		}

		if(!myPid) {
			console.error('No pid file for instance %s (are you sure it is running?)'.red, instanceDir);
			return restartOrExit(cmd, doRestart, 1, instanceDir);
		}

		ps.kill(myPid, 'SIGTERM', function(err) {
			if(err) {
				console.error('Failed to send SIGTERM to process %s'.red, myPid);
				console.error(err);
				return restartOrExit(cmd, doRestart, 1, instanceDir);
			}
			console.log('Successfully sent SIGTERM to pid %s', myPid);
			return restartOrExit(cmd, doRestart, 0, instanceDir);
		});
	}

};


exports.restart = function(name, cmd) {
	stop(name, cmd, true);
};


exports.pm2Ecosystem = function(name, cmd) {
	
	const instances = common.instanceIndex;
	const ecoObj = { apps:[] };

	Object.keys(instances).sort().forEach(instanceName=>{

		let instanceDir = instances[instanceName];

		ecoObj.apps.push({
			name:instanceName,
			script: './lib/server.js',
			cwd:NOONIAN_HOME,
			args:`--instanceDir ${instanceDir}`,
			out_file:path.resolve(instanceDir, 'stdout.log'),
			error_file:path.resolve(instanceDir, 'stderr.log'),
			pid_file:path.resolve(instanceDir, 'pm2_pid'),
			env:{
				NOONIAN_HOME,
				NOONIAN_INSTANCE: instanceDir,
				NODE_PATH: path.resolve(instanceDir, 'node_modules')
			}
		});
	}); 

	console.log(JSON.stringify(ecoObj, null, 3));

}