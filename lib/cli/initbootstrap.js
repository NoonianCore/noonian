const fs = require('fs');
const path = require('path');
const {fork} = require('child_process');

const uuid = require('uuid/v4');
const Q = require('q');

const common = require('./common');


const NOONIAN_HOME = common.NOONIAN_HOME;
const NOONIAN_USER_HOME = common.NOONIAN_USER_HOME;
const getInstanceDir = common.getInstanceDir;
const getInstanceName = common.getInstanceName;
const saveInstanceIndex = common.saveInstanceIndex;
const instances = common.instanceIndex;

const findUnusedPort = function() {
	const configs = common.getAllConfigs();
	var maxPort = 0;
	Object.keys(configs).forEach(n=>{
		let p = configs[n] && configs[n].serverListen.port;
		if(p > maxPort) {
			maxPort = p;
		}
	});

	return maxPort ? maxPort+1 : 9000;
};

//Copy utf-8 file from template directory; first check ~/.noonian, fall back to NOONIAN_HOME/template
const copyTemplateFile = function(instanceDir, src, dest, replace) {

	dest = dest || src; //dest optional; default to same file name

	var fileSrc = path.resolve(NOONIAN_USER_HOME, 'template', src);

	try {
		fs.statSync(fileSrc);
	} catch(err) {
		fileSrc = path.resolve(NOONIAN_HOME, 'template', src);
	}

	const fileDest = path.resolve(instanceDir, dest);

	var srcContent = fs.readFileSync(fileSrc, 'UTF-8');

	if(replace) {
		for(let k in replace) {
			srcContent = srcContent.replace(k, replace[k]);
		}
	}

	return fs.writeFileSync(fileDest, srcContent);
};

exports.init = function(name, cmd) {
	const instanceDir = path.resolve(cmd.directory || '.');
	const existing = getInstanceName(instanceDir);
	if(existing) {
		console.error('Instance "%s" already initialized in directory %s'.red, existing, instanceDir);
		process.exit(1);
	}

	//Create instanceDir if doesn't exist
	if (!fs.existsSync(instanceDir)){
	    fs.mkdirSync(instanceDir);
	}


	const dbName = cmd.db || 'noonian-'+name;
	const instanceId = cmd.instanceId || name;
	const sessionSecret = uuid();

	const listenPort = cmd.listenPort || findUnusedPort();
	const listenAddress = cmd.listenAddress || '127.0.0.1';

	const dbAddress = cmd.dbAddress || process.env.MONGO_ADDRESS || 'localhost';
	const databaseUri = cmd.dbUri || `mongodb://${dbAddress}/${dbName}`;

	//Instance Configuration file
	copyTemplateFile(instanceDir, 'instance-config.js', null, {
		'#LISTEN_PORT#': listenPort,
		'#LISTEN_ADDRESS#':listenAddress,
		'#INSTANCE_NAME#': name,
		'#INSTANCE_ID#': instanceId,
		'#DATABASE_URI#': databaseUri,
		'#SESSION_SECRET#': sessionSecret
	});

	copyTemplateFile(instanceDir, 'bower.json', null, {
		'#INSTANCE_NAME#':name
	});

	copyTemplateFile(instanceDir, 'bowerrc', '.bowerrc');
	copyTemplateFile(instanceDir, 'gitignore', '.gitignore');


	//Add this instance to the index
	instances[name] = instanceDir;
	saveInstanceIndex();



	console.log('Successfully initialized %s', name);
	console.log('Edit instance-config.js to configure, and then run "noonian bootstrap %s" to bootstrap the database.', name);
	process.exit(0);
};


exports.bootstrap = function(name, cmd) {

	var pwPromise;

	if(cmd.password) {
		if(typeof cmd.password === 'string') {
			pwPromise = Q(cmd.password);
		}
		else {
			//Prompt for admin password
			const deferred = Q.defer();
			pwPromise = deferred.promise;

			const rl = require('readline').createInterface(process.stdin, process.stdout);
			rl.setPrompt('Admin Password: ');
			rl.prompt();
			rl.on('line', line=>{
				if(line) {
					deferred.resolve(line);
					rl.close();
				}
				else {
					console.error('Aborting bootstrap');
					process.exit(1);
				}

			});
		}
	}
	else {
		pwPromise = Q(false);
	}

	return pwPromise.then(adminPw=>{

		const instanceDir = getInstanceDir(name, cmd);

		if(instanceDir) {
			console.log('bootstrapping %s', instanceDir);


			const env = process.env;
			env.NODE_PATH = path.resolve(instanceDir, 'node_modules');
			env.NOONIAN_HOME = NOONIAN_HOME;
			env.NOONIAN_INSTANCE = instanceDir;

			if(adminPw) {
				env.ADMIN_PW = adminPw;
			}

			if(cmd.verbose) {
				console.log('NODE_PATH=%s', env.NODE_PATH);
				console.log('NOONIAN_HOME=%s', NOONIAN_HOME);
				console.log('ADMIN_PW=%s', adminPw);
			}

			const childProc = fork(
				path.resolve(NOONIAN_HOME, 'lib/bootstrap.js'), 
				[], 
				{
					cwd:NOONIAN_HOME,
					stdio:'inherit',
					env:env
				}
			);
		}

	},
	err=>{console.error(err); process.exit(1);});
};