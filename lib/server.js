/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/**
* server/app.js
*   SERVER ENTRY POINT
**/

const fs = require('fs');
const path = require('path');

const express = require('express');
const _ = require('lodash');


const serverConfig = require('./conf');





serverConfig.init();

const logger = require('./api/logger').get('sys');

logger.info('Starting Noonian instance %s', serverConfig.instanceName);


const pidFile = path.resolve(serverConfig.instanceDir, 'pid');


const terminate = function(status, signal) {
  if(signal) {
    logger.info(`terminating due to ${signal}...`);
  }
  fs.unlinkSync(pidFile);
  process.exit(status);
};

const gracefulTerminate = terminate.bind(null, 0);

process.on('SIGINT', gracefulTerminate);
process.on('SIGTERM', gracefulTerminate);


const app = express();

if(serverConfig.expressSettings) {
    logger.verbose('including settings for express.js: %j', serverConfig.expressSettings);
    _.forEach(serverConfig.expressSettings, function(val, key) {
        app.set(key, val);
    });
}

// Setup server
const setupServer = function() {

  var server;

  if(!serverConfig.useHttps) {
    logger.verbose("SETTING UP HTTP SERVER");
	   server = require('http').createServer(app);
  }
  else {
    logger.verbose("SETTING UP HTTPS SERVER");
    var httpsOpts = {
      key: fs.readFileSync(serverConfig.ssl.keyFile),
      cert: fs.readFileSync(serverConfig.ssl.certFile)
    };
    server = require('https').createServer(httpsOpts, app);
  }

	logger.verbose("CONFIGURING EXPRESS");
	require('./conf/express')(app);

  logger.verbose("SETTING UP ROUTES");
	require('./routes')(app);
    
    
  if(serverConfig.enableWebSockets) {
      logger.verbose("SETTING UP WEB SOCKETS");
      require('./websockets.js')(server);
  }

	// Start server
	logger.verbose("STARTING SERVER");
	server.listen(serverConfig.serverListen.port, serverConfig.serverListen.host, function () {
    fs.writeFileSync(pidFile, ''+process.pid);
	  logger.info('%s server listening on %s %d', (serverConfig.useHttps ? 'HTTPS' : 'http'), (serverConfig.serverListen.host ? serverConfig.serverListen.host+':' : ''), serverConfig.serverListen.port);
	});

}

// Expose app
exports = module.exports = app;


//Initialize server components:
require('./api/datasource').init(serverConfig)
  .then(require('./api/schedule').init)
  .then(
    setupServer,
    function(err) {
      logger.error("ERROR INITIALIZING DATASOURCE").error(err);  
      terminate(1);
    }
  )
  ;




