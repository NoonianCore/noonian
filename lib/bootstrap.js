/*
Copyright (C) 2016-2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/**
* lib/bootstrap.js
*   Bootstrap mongo database for a noonian instance. 
**/


const serverConfig = require('./conf');
serverConfig.init();
serverConfig.displayInfo();


//Initialize server components:
var db = require('./api/datasource');

db.bootstrapDatabase(process.env.ADMIN_PW).then(function() {
  console.log('Bootstrap Complete.');
  process.exit(0);
});
