/*
Copyright (C) 2017-2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
/*
 * schedule.js
 * 
 */
var _ = require('lodash');
var Q = require('q');

var scheduler = require('node-schedule');

var db = require('./datasource');
var DataTriggerService = require('./datasource/datatrigger');
var invokerTool = require('../util/invoker');

const logger = require('./logger').get('sys.schedule');

var scheduledJobs = {};

var executeScheduleTrigger = function(schedStr) {
    if(this.running) {
        logger.warn('Skipping execution of ScheduleTrigger %s (still running from previous execution)', this._id);
    }
    this.running = true;
    logger.info('Executing ScheduleTrigger %s on schedule [%s]', this.key, schedStr || 'manual invokation');
    
    var st = this;
    var fn = this.function;
    
    return invokerTool.invokeAndReturnPromise(fn, {schedStr}, this).then(
        function(r) {
            logger.verbose('Successful invocation of ScheduleTrigger %s; result: %j', st.key || st._id, r);
            delete st.running;
            st.last_execution = new Date();
            st.last_execution_result = r;
            st.last_execution_success = true;

            st.save({skipTriggers:true}, null);
        },
        function(err) {
            logger.error('Error executing ScheduleTrigger %s', st.key || st._id).error(err);
            delete st.running;

            st.last_execution = new Date();
            st.last_execution_success = false;

            let r = {};
            if(err.message) {
                r.error = err.message;
                r.stack = err.stack;   
            }
            else {
                r.error = err;
            }

            st.last_execution_result = r;

            st.save({skipTriggers:true}, null);
        }
    );
};


var installScheduleTrigger = function(st) {
    if(!st.function) {
        logger.error('Skipping loading scheduleTrigger %s (invalid function)', st._id);
        return;
    }


    
    var s = st.schedule;
    var schedStr =  s.second+' '+
          s.minute+' '+
          s.hour+' '+
          s.day_of_month+' '+
          s.month+' '+
          s.day_of_week;
    
    logger.verbose('Installing ScheduleTrigger %s with schedule [%s]', st.key||st._id, schedStr);


    scheduledJobs[st._id] = {
        job:scheduler.scheduleJob(schedStr, executeScheduleTrigger.bind(st, schedStr)),
        bo:st
    };
};

var cancelJob = function(st) {
    var installed = scheduledJobs[st._id];
    if(installed) {
        logger.debug('Cancelling job %s', st.key || st._id);
        installed.job && installed.job.cancel();
        delete scheduledJobs[st._id];
    }
};

exports.manualExecute = function(scheduleTriggerId) {
    var installed = scheduledJobs[scheduleTriggerId];
    if(installed && installed.bo) {
        return executeScheduleTrigger.call(installed.bo);
    }
    else {
        return db.ScheduleTrigger.findOne({_id:scheduleTriggerId}).then(st=>{
            if(st) {
                return executeScheduleTrigger.call(st);
            }
            else {
                throw 'Invalid ScheduleTrigger id: '+scheduleTriggerId;
            }
        })
    }
};

exports.init = function(conf) {
    if(!db.ScheduleTrigger) {
        logger.error('ScheduleTrigger not available; upgrade sys package!');
        return Q(true);
    }
    
    DataTriggerService.registerDataTrigger('sys.internal.scheduler', 'UfkDq2TKQAm4OWijwTpokQ', 'after', true, true, true, 
        function(isCreate, isDelete) {
            if(isCreate) {
                if(this.enabled) {
                    installScheduleTrigger(this);
                }
            }
            else {  //Update or Delete
                cancelJob(this);

                if(!isDelete && this.enabled) { 
                    installScheduleTrigger(this);
                }
            }
        }
    );
    
    return db.ScheduleTrigger.find({enabled:true}).then(function(resultArr) {
        _.forEach(resultArr, function(st) {
            installScheduleTrigger(st);
        });
    }); 
};
