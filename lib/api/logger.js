/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
/**
 * logger.js
 *  api for accessing noonian logger
 *  basic named loggers for now; Future features:
 *  - allow configuring different loggers to use different files/transports and levels
 *  - dynamic changing of logging config at runtime and/or w/in DBUI
 **/


const path = require('path');

const serverConf = require('../conf');


const winston = require('winston');
const { format, transports } = winston;
const { combine, timestamp, printf, colorize, splat } = format;



const toFullPath = function(filename) {
	if(path.isAbsolute) {
		return filename;
	}

	return path.resolve(serverConf.instanceDir||'', filename);
};


const textFormatFn = function (opts) {
	const { level, message, timestamp, loggerName } = opts;

	var logStr;

	if(loggerName) {
  		logStr = `${timestamp} [${loggerName}] ${level}: ${message}`;
	}
	else {
	  	logStr = `${timestamp} ${level}: ${message}`;
	}

	if(opts.stack) {
		return `${logStr}\n${opts.stack}`;
	}

	if(opts.pretty) {
		let pretty = JSON.stringify(opts.pretty, null, 2);
		return `${logStr}\n${pretty}`;
	}
	return logStr;

};

const getTextFormatter = function(loggerConf) {
	var fmtFn = textFormatFn;
	if(loggerConf) {
		fmtFn = textFormatFn.bind(loggerConf);
	}

	return combine(
		timestamp(),
		colorize(),
		splat(),
		printf(fmtFn)
	);
};



const loggerMap  = {}; //logger name -> winston logger object


/**
 * Retreive logger from map.  Logger name can be dotted path; will search for most specific logger applicable to loggerName
 *  e.g. if logger "sys" is configured, but "sys.abc" is not, getLogger will return logger for "sys"
*/
const getLogger = function(loggerName) {
	if(!loggerName) {
		return loggerMap.default;
	}

	let exactLogger = loggerMap[loggerName];
	if(exactLogger) {
		return exactLogger;
	}

	let parts = loggerName.split('.');
	if(parts.length === 1) {
		return loggerMap.default;
	}

	parts.pop();	
	let parentLoggerName = parts.join('.');
	let parentLogger = getLogger(parentLoggerName);
	return parentLogger.child({loggerName});

};

const createLogger = function(conf, nameSpec) {
	const names = nameSpec.split(',');

	const loggerConf = {
		format:getTextFormatter(conf),
		defaultMeta:{},
		transports:[]
	};

	if(conf.level) {
		loggerConf.level = conf.level;
	}

	if(conf.file) {
		loggerConf.transports.push(
			new transports.File({filename:toFullPath(conf.file)})
		);
	}

	_.forEach(conf.files, (filename, level)=>{
		loggerConf.transports.push(
			new transports.File({filename:toFullPath(filename), level})
		);
	});

	const loggerObj = winston.createLogger(loggerConf);
	names.forEach(n=>{
		if(n === '*') {  //alias *
			n = 'default';
		}
		loggerMap[n] = loggerObj;
	});

};

if(serverConf.loggers) {
	_.forEach(serverConf.loggers, createLogger);
}
else {
	 const rootLogger = loggerMap.root = winston.createLogger({
		level:serverConf.logLevel || 'debug',
		format: getTextFormatter(),
		defaultMeta:{},

		transports:[
			new transports.File({filename:path.resolve(serverConf.instanceDir||'', 'instance.log')}),
			new transports.File({filename:path.resolve(serverConf.instanceDir||'', 'error.log') ,level:'error'})
		]

	});

	 loggerMap.default =  rootLogger.child({loggerName:(serverConf.defaultLoggerName||'')});
}


const rootLogger = loggerMap.root;

const defaultLogger =  module.exports = loggerMap.default;

module.exports.get = function(loggerName) {
	if(!loggerName) {
		return defaultLogger;
	}

	return rootLogger.child({loggerName});
};
