/*
Copyright (C) 2016-2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
/**
 * datatrigger.js
 * All functionality to respond to data-update events to DataTrigger's
 */
const Q = require('q');
const _ = require('lodash');
const db = require('./index');

const invokerTool = require('../../util/invoker');

const appConfig = require('../../conf');

const METADATA = Symbol.for('metadata');

const logger = require('../logger').get('sys.db.datatrigger');


//Each before/after "bucket" maps BOD id to the applicable data triggers; the triggers divided into 3 lists: onCreate,onUpdate,onDelete
const dataTriggers = exports._dataTriggerMap = {
  before:{
    '*':{onCreate:[], onUpdate:[], onDelete:[]}
  },
  after:{
    '*':{onCreate:[], onUpdate:[], onDelete:[]}
  }
};

const dtBackrefs = exports._dataTriggerBackrefs = {}; //maps DataTrigger id to the onCreate/onUpdate/onDelete arrays that contain it


/**
  Register a DataTrigger object
*/
const registerDataTriggerObj = function(dt, useBodId) {
  
  const bucket = dataTriggers[dt.before_after];
  const bodId = useBodId || (dt.business_object && dt.business_object._id ? dt.business_object._id : '*');

  logger.debug('registerDataTriggerObj: %s  %s', dt.key, bodId);

  const dtId = dt._id || 'none';
  dt.priority = dt.priority || 0;

  if(!bucket[bodId])
    bucket[bodId] = {onCreate:[], onUpdate:[], onDelete:[]};

  if(!dtBackrefs[dtId])
    dtBackrefs[dtId] = [];

  var arr;
  if(dt.on_create) {
    arr = bucket[bodId].onCreate;
    arr.push(dt);
    dtBackrefs[dtId].push(arr);
  }
  if(dt.on_update) {
    arr = bucket[bodId].onUpdate;
    arr.push(dt);
    dtBackrefs[dtId].push(arr);
  }
  if(dt.on_delete) {
    arr = bucket[bodId].onDelete;
    arr.push(dt);
    dtBackrefs[dtId].push(arr);
  }


  //If there are any child classes of this object, we want them connected to this data trigger as well:
  if(bodId !== '*' && db[bodId]) {
    const myMeta = db[bodId][METADATA];
    if(myMeta.child_models && myMeta.child_models.length) {
      myMeta.child_models.forEach(cm=>{
        let childMd = cm[METADATA];
        logger.debug('registering child model %s', childMd.class_name);
        registerDataTriggerObj(dt, childMd.bod_id)
      });
    }
  }

};

/**
 * Remove a DataTrigger object from the cache
*/
const unregisterDataTrigger = function(dtId) {
  logger.debug('unregister dataTrigger: %s', dtId);

  if(dtBackrefs[dtId]) {
    _.forEach(dtBackrefs[dtId], function(containingArray) {
      for(var i=0; i < containingArray.length; i++) {
        if(containingArray[i]._id === dtId) {
          containingArray.splice(i, 1);
          i--;
        }
      }
    });
    dtBackrefs[dtId] = [];
  }

}

//For system-level triggers (triggers that are not DataTrigger business objects)
var registerDataTrigger =
exports.registerDataTrigger = function(key, bodId, beforeAfter, onCreate, onUpdate, onDelete, actionFn, priority) {
  registerDataTriggerObj({
    systemLevel:true,
    key:key,
    business_object:(bodId  ? {_id:bodId} : null),
    before_after:beforeAfter,
    on_create:onCreate,
    on_update:onUpdate,
    on_delete:onDelete,
    action:actionFn,
    priority: (priority || 0)
  });
};


const clearNonSystemTriggers = function() {
  ['before','after'].forEach(ba=>{
    const bucket = dataTriggers[ba];
    _.forEach(bucket, (triggerMap, bodId)=>{
      //triggerMap is the {onCreate:[...], onDelete:[...], onUpdate:[...]}
      _.forEach(triggerMap, triggerList=>{
        for(let i=0; i < triggerList.length; i++) {
          let dt = triggerList[i];
          if(!dt.systemLevel) {
            triggerList.splice(i, 1);
            i--;
          }
        }
      })
    });
  })
};

const refreshDataTriggers = 
exports.refreshDataTriggers = function() {

  logger.info('Refreshing DataTriggers');
  return db.DataTrigger.find({}).exec().then(function(dtList) {
    clearNonSystemTriggers();
    _.forEach(dtList, dt=>{
      unregisterDataTrigger(dt._id);
      registerDataTriggerObj(dt);
    });
  });
};


const onDataTriggerUpdate = function(isCreate, isDelete) {
  if(!isCreate)
    unregisterDataTrigger(this._id);
  if(!isDelete)
    registerDataTriggerObj(this);
}

exports.init = function() {
  registerDataTrigger('sys.internal.dataTriggerCacheUpdate', 'w1FEKYa4SbiVPVqAtEWJyw', 'after', true, true, true, onDataTriggerUpdate);
  return refreshDataTriggers();

};


const Invoker = function(dataTrigger, injectables, obj, collectedResults, breakChain) {
  collectedResults = collectedResults || {};
  this.invoke = ()=>{
    const deferred = Q.defer();

    logger.verbose('invoking DataTrigger %s for %s.%s', dataTrigger.key, obj._bo_meta_data.class_name, obj._id);
    this.$invocationPromise = invokerTool.invokeAndReturnPromise(dataTrigger.action, injectables, obj).then(
      result =>{
        logger.debug('successful invocation of DataTrigger %s', dataTrigger.key);
        logger.silly(result);
        collectedResults[dataTrigger.key] = result;
        deferred.resolve(result);
      },
      err => {
        logger.error('error invoking DataTrigger %s', dataTrigger.key).error(err);
        collectedResults[dataTrigger.key] = err;
        if(breakChain) {
          deferred.reject(err);
        }
        else {
          deferred.resolve(err);
        }
      }
    );

    return deferred.promise;
  }

};

var processTriggers = function(bodId, beforeAfter, createUpdateDelete, modelObj, keyFilter, saveOptions) {
  
  const isBefore = (beforeAfter==='before');

  var promiseChain = Q(true);

  var bucket = dataTriggers[beforeAfter];

  var dtList = bucket['*'][createUpdateDelete] || [];

  if(bucket[bodId])
    dtList = dtList.concat(bucket[bodId][createUpdateDelete]);

  dtList = _.sortBy(dtList, 'priority');

  var filterRegex = false;
  if(keyFilter) {
    try {
      filterRegex = new RegExp(keyFilter);
    } catch (err) {
      logger.warn('invalid keyFilter in processTriggers: %s', keyFilter);
    }
  }


  const invocationResults = {};
  
  const injectables = {
    id:modelObj._id,
    isUpdate:(createUpdateDelete==='onUpdate'),
    isCreate:(createUpdateDelete==='onCreate'),
    isDelete:(createUpdateDelete==='onDelete'),
    saveOptions:saveOptions,
    invocationResults
  };


  _.forEach(dtList, function(dt){

    if(filterRegex && !filterRegex.test(''+dt.key)) {
      logger.verbose('SKIPPING DataTrigger %s (not matching regex %s)', dt.key, keyFilter);
      return;
    }


    let invoker = new Invoker(dt, injectables, modelObj, invocationResults, isBefore);
    promiseChain = promiseChain.then(invoker.invoke);
  });


  return promiseChain.then(()=>invocationResults);
};



exports.processBeforeCreate = function(modelObj, keyFilter, saveOptions) {
  return processTriggers(modelObj._bo_meta_data.bod_id, 'before', 'onCreate', modelObj, keyFilter, saveOptions);
};

exports.processBeforeUpdate = function(modelObj, keyFilter, saveOptions) {
  return processTriggers(modelObj._bo_meta_data.bod_id, 'before', 'onUpdate', modelObj, keyFilter, saveOptions);
};

exports.processBeforeDelete = function(modelObj, keyFilter, saveOptions) {
	return processTriggers(modelObj._bo_meta_data.bod_id, 'before', 'onDelete', modelObj, keyFilter, saveOptions);
};



exports.processAfterCreate = function(modelObj, keyFilter, saveOptions) {
	return processTriggers(modelObj._bo_meta_data.bod_id, 'after', 'onCreate', modelObj, keyFilter, saveOptions);
};

exports.processAfterUpdate = function(modelObj, keyFilter, saveOptions) {
  return processTriggers(modelObj._bo_meta_data.bod_id, 'after', 'onUpdate', modelObj, keyFilter, saveOptions);
};

exports.processAfterDelete = function(modelObj, keyFilter, saveOptions) {
	return processTriggers(modelObj._bo_meta_data.bod_id, 'after', 'onDelete', modelObj, keyFilter, saveOptions);
};
