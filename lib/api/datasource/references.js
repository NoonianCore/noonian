/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
/**
 * references.js
 *  Service to deal with/maintain reference fields
 **/
const Q = require('q');
const _ = require('lodash');

const db = require('./index');
const DataTriggerService = require('./datatrigger');
const GridFsService = require('./gridfs');

const logger = require('../logger').get('sys.db.references');

const mongoose = require('mongoose');

//Maintain a special collection noonian.references efficiently handle references bidirectionally
const IncomingRefSchema = new mongoose.Schema({
    target_id:{
      type: String,
      index: true
    },
    target_class: String,
    referencing_class: String,
    referencing_id: {
      type: String,
      index: true
    },
    referencing_field: String,
    referenced_from_array: Boolean
  },
  {collection:'noonian.references'}
);


var IncomingRefModel; //mongoose model is initialized after db is ready


const initRefModel = function() {
  const conn = db.mongooseConnections.system || db.mongooseConnections.default;
  IncomingRefModel = conn.model('IncomingRef', IncomingRefSchema);
};


/**
 * Set _disp and denormalized fields for a reference value
 *  @this bound to the reference field struct  {_id:'...', _disp:'...'}
 *  refObj is current version of referenced object
 **/
const augmentRef = function(modelObj, fieldName, td, refObj, isDelete) {
  if(this && refObj) {
    if(isDelete) {
      this._disp = (this._disp || '')+'(deleted)';
      this.deleted = true;
    }
    else {
      this._disp = refObj._disp;
    }
    

    if(td.denormalize_fields) {
      for(var i=0; i < td.denormalize_fields.length; i++) {
        var df = td.denormalize_fields[i];
        this[df] = refObj[df];
      }
    }
    

    //Since we're changing values on an object w/out changing the object 
    // itself, need to make sure Mongoose knows to persist
    modelObj.markModified(fieldName);
  }
  
  return refObj;
}

const registerRef = function(fromClass, fromId, fromField, toClass, toId, isArray) {
  var irSpec = {
    target_class:toClass,
    target_id:toId,
    referencing_class:fromClass,
    referencing_id:fromId,
    referencing_field:fromField,
    referenced_from_array:!!isArray
  };

  return IncomingRefModel.count(irSpec).exec().then(function(matchCount) {
    if(matchCount === 0) {
      return new IncomingRefModel(irSpec).save();
    }
  });
};


/**
 * @return minimal reference stub built from provided reference field value
 */
const createRefStub = function(td, fieldValue) {
  
  var stub = {_id:fieldValue._id, _disp:fieldValue._disp}; 
  
  //handle "generic" reference wherein ref_class is specified in value instead of type descriptor
  if(!td.ref_class) {
    stub.ref_class = fieldValue.ref_class;
  }
  
  return stub;
}

const getRefObj = function(refValue, td) {
  let refClass = td.ref_class || refValue.ref_class;
  let refId = refValue._id;

  return db[refClass].findById(refId);
};

//Recurse into tdMap, make sure each outgoing ref gets augmented via augmentRef
const augmentOutgoingRefs = function(modelObj, tdMap, contextObj, baseField, fieldPrefix) {
  tdMap = tdMap || modelObj._bo_meta_data.type_desc_map;
  contextObj = contextObj || modelObj;
  fieldPrefix = fieldPrefix || '';

  const myClassName = modelObj._bo_meta_data.class_name;
  const myId = modelObj._id;

  var promiseChain = Q(true);

  _.forEach(tdMap, (td, fieldName) => {
    let isArray = td instanceof Array;
    td = isArray ? td[0] : td;
    let fieldValue = _.get(contextObj, fieldName);

    //Pack single ref into an array to simplify processing logic
    let valueArr = isArray ? fieldValue : [fieldValue];

    if(td.type === 'reference' && fieldValue) {
      
      valueArr.forEach(refValue=>{
        if(refValue) {
          promiseChain = promiseChain
            .then(getRefObj.bind(null, refValue, td))
            .then(augmentRef.bind(refValue, modelObj, baseField || fieldName, td))
            .then(registerRef.bind(null, myClassName, myId, fieldPrefix+fieldName, td.ref_class||refValue.ref_class, refValue._id));
          }
      });
    }
    else if(td.is_composite && fieldValue) {
      valueArr.forEach(compObj=>{
        if(compObj) {
          promiseChain = promiseChain.then(
            augmentOutgoingRefs(modelObj, td.type_desc_map, compObj, baseField || fieldName, fieldPrefix+fieldName+'.')
          );
        }
      });
    }

  });

  return promiseChain;

};




/**
 * When saving a BusinessObject to the DB, process it's reference fields so they are up-to-date and properly formatted.
 *  Do it here rather than in FieldType.to_db becuase it requires further DB interaction
 **/
const processOutwardRefs = function(isUpdate, isDelete) {
  var modelObj = this;
  var myClassName = modelObj._bo_meta_data.class_name;
  var typeDesc = modelObj._bo_meta_data.type_desc_map;
  
  
  //First, clear out records of outgoing references from modelObj
  return IncomingRefModel.remove({referencing_class:myClassName,referencing_id:modelObj._id}).then(function() {
    var promises = [];
  
    if(!isDelete) { 
      //modelObj was just created or updated
      promises.push(augmentOutgoingRefs(modelObj));
      
      _.forEach(typeDesc, function(td, fieldName) {
        var fieldValue = modelObj[fieldName];
        if(td.type == 'attachment' && fieldValue) {
          var attId = fieldValue.attachment_id;
          GridFsService.annotateIncomingRef(attId, myClassName, modelObj._id, fieldName);
          //TODO clean up attachment annotations for _previous value
        }
        else if(td instanceof Array && td[0].type === 'attachment' && fieldValue && fieldValue.length) {
          for(var i=0; i < fieldValue.length; i++) {
            var attObj = fieldValue[i];
            if(attObj && attObj.attachment_id) {
              GridFsService.annotateIncomingRef(attObj.attachment_id, myClassName, modelObj._id, fieldName);
            }
          }
          //TODO clean up attachment annotations for _previous value
        }
        
      }); //end typedesc iteration
    }

    return Q.all(promises).then(function(){return true;});
  });
};


/**
 *  Given fieldPath, dig into contextObj and return the value.
 *  Handles arrays along the way, e.g. reference in a composite array, reference array in a composite, etc.
 *  @return an array of values
 */
const resolveValue = function(contextObj, fieldPath) {
  const dotPos = fieldPath.indexOf('.');
  if(dotPos < 0) {
    let val = contextObj && contextObj[fieldPath];

    return val instanceof Array ? val : [val];
  }

  const baseField = fieldPath.substring(0, dotPos);
  const subPath = fieldPath.substring(dotPos+1);

  var valArray = contextObj && contextObj[baseField];

  valArray = valArray instanceof Array ? valArray : [valArray];

  const ret = [];
  valArray.forEach(v=>{
    resolveValue(v, subPath).forEach(resolved=>{
      ret.push(resolved);
    })
  });
  return ret;

};

/**
 * Replace any reference to replaceId in contextObj @ specified fieldPath with replacementRef
 *  @return boolean indicating if value was found and replaced.
**/
const replaceRef = function(contextObj, fieldPath, replaceId, replacementRef) {
  const dotPos = fieldPath.indexOf('.');
  if(dotPos < 0) {
    let val = contextObj && contextObj[fieldPath];

    if(val instanceof Array) {
      //Find replaceId
      let foundAny = false;
      for(let i=0; i < val.length; i++) {
        let r = val[i];
        if(r && r._id === replaceId) {
          val[i] = replacementRef;
          foundAny = true;
        }
      }
      return foundAny;
    }
    else {
      if(val._id === replaceId) {
        contextObj[fieldPath] = replacementRef;
        return true;
      }
    }

    return false;
  }

  const baseField = fieldPath.substring(0, dotPos);
  const subPath = fieldPath.substring(dotPos+1);

  var baseVal = contextObj && contextObj[baseField];

  if(baseVal instanceof Array) {
    //traverse into each element of composite array
    var foundAny = false;
    baseVal.forEach(baseValItem=>{
      var result = replaceRef(baseValItem, subPath, replaceId, replacementRef);
      foundAny = foundAny || result;
    });
    return foundAny;
  }
  else {
    return replaceRef(contextObj[baseField], subPath, replaceId, replacementRef);
  }
};


/**
 * When a BO is saved or deleted, trace back all references to it, and ensure the _disp and denormalized fields are updated
 */
const processInwardRefs = function(isDelete) {
  var modelObj = this;
  var myClassName = modelObj._bo_meta_data.class_name;

  //Look up all references to modelObj
  return IncomingRefModel.find({target_id: modelObj._id, target_class: myClassName}).then(function(irefs) {
    
    if(!irefs || irefs.length === 0) return;
    
    var promises = [];
    _.forEach(irefs, function(iref) {
      var refClass = iref.referencing_class;
      var refId = iref.referencing_id;
      var refField = iref.referencing_field;
      
      if(!db[refClass]) {
        logger.error('[REF-REPAIR] Bad incoming reference class %s in %s.%s', refClass, myClassName, modelObj._id);
        return;
      }

      //Grab the referencing object...
      var inrefPromise = db[refClass].findById(refId).then(function(bo) {
        if(!bo) return;

        const refList = resolveValue(bo, refField);
        var refTd = bo._bo_meta_data.getTypeDescriptor(refField);

        const isArray = refTd instanceof Array;
        refTd = isArray ? refTd[0] : refTd;

        let dotPos = refField.indexOf('.');
        let baseField = (dotPos > 0 ? refField.substring(0, dotPos) : refField);

        if(!isDelete || !refTd.on_delete || refTd.on_delete === 'no action') {
          //It's not a delete, or the on-delete specifier in referencing typedesc says to do nothing.

          resolveValue(bo, refField).forEach(refObj=>{
            if(refObj && refObj._id === modelObj._id) {
              augmentRef.call(refObj, bo, baseField, refTd, modelObj, isDelete);
            }
          });
          
          //Save referencing object w/out affecting it's version or triggering data triggers
          return bo.save({useVersionId:bo.__ver, skipTriggers:true},null);
        }
        else if(refTd.on_delete === 'cascade') {
          //Double-check to make sure ref exists in BO 
          // (if index is outdated we don't want to cascade the delete for object that USED to refer to this one)
          var found = false;
          resolveValue(bo, refField).forEach(refObj=>{
            if(refObj && refObj._id === modelObj._id) {
              found = true;
            }
          });
          if(found) {
            return bo.remove();
          }

        }
        else if(refTd.on_delete === 'set null' || refTd.on_delete === 'default') {
          const setVal = refTd.on_delete === 'default' ? refTd.default_value : null;

          var replaced = replaceRef(bo, refField, modelObj._id, setVal) ;

          if(replaced) {
            bo.markModified(baseField);
          }

          //Save referencing object w/out affecting it's version or triggering data triggers
          return bo.save({useVersionId:bo.__ver, skipTriggers:true},null);
        }

      });
      
      promises.push(inrefPromise);

    });//end iteration through irefs
    
    
    var finalPromise = Q.all(promises);
    
    if(isDelete) {
      //When all is said and done, clear out records of references to modelObj
      finalPromise = finalPromise.then(function() {
        return IncomingRefModel.remove({target_id: modelObj._id, target_class: myClassName})
      });
    }
    
    //Ensure returned promise is fulfilled when all incoming refs are processed
    return finalPromise;
  });
}

const handleOneToOne = function(modelObj, fieldName) {
  //if my reference changed:
  //  query for target:
  //  if target back ref isn't pointing to me, then update and save 
  //  if _previous value is not null (reference changed from something else)
  //    query for _previous target:
  //    if _previous target back ref isn't null, update it to null and save.
  var myRef = modelObj[fieldName] || {};
  var myPrevRef = (modelObj._previous && modelObj._previous[fieldName]) || {};
  
  if(myRef._id == myPrevRef._id) {
    //No change; we're done
    return Q(true);
  }
    
  var myClassName = modelObj._bo_meta_data.class_name;
  var myTypeDescMap = modelObj._bo_meta_data.type_desc_map;
  var myTd = myTypeDescMap[fieldName];
  
  var targetClassName = myTd.ref_class;
  var targetTypeDescMap = db[targetClassName]._bo_meta_data.type_desc_map;
  var targetTd = targetTypeDescMap[myTd.back_ref]; 
  
  var promises = [];
  
  if(myRef._id) {
    promises.push(db[targetClassName].findOne({_id:myRef._id}));
  }
  else {
    promises.push(Q(null));
  }
  if(myPrevRef._id) {
    promises.push(db[targetClassName].findOne({_id:myPrevRef._id}));
  }
  else {
    promises.push(Q(null));
  }
  
  return Q.all(promises).then(function(resultArr) {
    var toResolve = [];
    var currTarget = resultArr[0];
    var prevTarget = resultArr[1];
    var backRefField = myTd.back_ref;
    if(currTarget) {
      //make sure currTarget[backRefField] points back to modelObj
      let backRef = currTarget[backRefField];
      if(!backRef || backRef._id !== modelObj._id) {
        logger.silly('Updating backref for %s.%s -> %s.%s', myClassName, fieldName, targetClassName, backRefField);
        currTarget[backRefField] = {_id:modelObj._id};
        toResolve.push(currTarget.save());
      }
    }
    
    if(prevTarget) {
      //clear out prevTarget[backRefField] if it still points to me
      let backRef = prevTarget[backRefField];
      if(backRef && backRef._id === modelObj._id) {
        logger.silly('Nulling prev backref for %s.%s -> %s.%s', myClassName, fieldName, targetClassName, backRefField);
        prevTarget[backRefField] = null;
        toResolve.push(prevTarget.save());
      }
    }
    
    return Q.all(toResolve);
  });
};

const handleManyToMany = function(modelObj, fieldName, isCreate, isUpdate, isDelete) {
  //Find delta between my _previous and current list of references
  //  query for all targets in delta
  //  for each REMOVED
  //    check that backref field in target doesn't include me; if it does, remove and save
  //  for each ADDED
  //    check that backref field in target includes me; if it doesn't add and save
  
  var prevRefs = _.map((modelObj._previous && modelObj._previous[fieldName]), '_id');
  var currRefs = _.map(modelObj[fieldName], '_id');
  var removed = _.difference(prevRefs, currRefs);
  var added = _.difference(currRefs, prevRefs);
  
  if(!removed.length && !added.length) {
    //no change; we're done
    return Q(true);
  }
  
  
  
  var myClassName = modelObj._bo_meta_data.class_name;
  var myTypeDescMap = modelObj._bo_meta_data.type_desc_map;
  var myTd = myTypeDescMap[fieldName][0];
  var backrefField;
  
  
  var targetClassName = myTd.ref_class;
  var targetTypeDescMap = db[targetClassName]._bo_meta_data.type_desc_map;
  var targetTd = targetTypeDescMap[myTd.back_ref][0]; 

  var promises = [];
  
  if(removed.length) {
    promises.push(db[targetClassName].find({_id:{$in:removed}}));
  }
  else {
    promises.push(Q([]));
  }
  if(added.length) {
    promises.push(db[targetClassName].find({_id:{$in:added}}));
  }
  else {
    promises.push(Q([]));
  }
  
  return Q.all(promises).then(function(resultArr) {
    var toResolve = [];
    var removedObjects = resultArr[0];
    var addedObjects = resultArr[1];
    var backRefField = myTd.back_ref;
    
    _.forEach(removedObjects, function(target) {
      //check that backref field in target doesn't include me; if it does, remove and save
      var backrefIds = _.map(target[backRefField], '_id');
      var myPos = backrefIds.indexOf(modelObj._id);
      var modified = false;
      while(myPos > -1) {
        modified = true;
        target[backRefField].splice(myPos, 1);
        backrefIds.splice(myPos, 1);
        myPos = backrefIds.indexOf(modelObj._id);
      }
      if(modified) {
        logger.silly('Updating REMOVED backref for %s.%s -> %s.%s', myClassName, fieldName, targetClassName, backRefField);
        target.markModified(backRefField);
        toResolve.push(target.save());
      }
    });
    
    _.forEach(addedObjects, function(target) {
      //check that backref field in target includes me; if it doesn't add and save
      var backrefIds = _.map(target[backRefField], '_id');
      var myPos = backrefIds.indexOf(modelObj._id);
      if(myPos < 0) {
        target[backRefField] = target[backRefField] ? _.clone(target[backRefField]) : [];
        target[backRefField].push({_id:modelObj._id});
        logger.silly('Updating ADDED backref for %s.%s -> %s.%s', myClassName, fieldName, targetClassName, backRefField);
        toResolve.push(target.save());
      }
    });
    
    return Q.all(toResolve);
  });
  
};



const processBackRefs = function(isCreate, isUpdate, isDelete) {
  var modelObj = this;
  var myClassName = modelObj._bo_meta_data.class_name;
  var typeDesc = modelObj._bo_meta_data.type_desc_map;
  
  var promises = [];
  
  _.forEach(typeDesc, function(td, fieldName) {
    var isArray = td instanceof Array;
    td = isArray ? td[0] : td;
    
    if(td.type === 'reference' && td.back_ref) {
      //Are we talking about one-to-one or many-to-many? 
      var targetTd = db[td.ref_class]._bo_meta_data.type_desc_map[td.back_ref];
      var targetIsArray = targetTd instanceof Array;
      if(targetIsArray && isArray) {
        promises.push(handleManyToMany(modelObj, fieldName, isCreate, isUpdate, isDelete));
      }
      else if(!targetIsArray && !isArray) {
        promises.push(handleOneToOne(modelObj, fieldName, isCreate, isUpdate, isDelete));
      }
      else {
        logger.error('one-to-many backref not yet supported! %s.%s - %s.%s', myClassName, fieldName, td.ref_class, td.back_ref);
      }
    }
    
  });
  
  return Q.all(promises);
};



exports.init = function(conf) {
  logger.info('initializing reference service');
  
  initRefModel();
  

  //Register data trigger for outgoing refs - prepare so proper denormalized fields; 100 priorty to occurr after other 'before' datatriggers
  DataTriggerService.registerDataTrigger('sys.internal.processOutRefs', null, 'before', true, true, true, processOutwardRefs, 100);

  //Register data trigger for incoming refs; -100 priorty so it happens before other 'after' datatriggers
  DataTriggerService.registerDataTrigger('sys.internal.processInRefs', null, 'after', false, true, true, processInwardRefs, -100);
  
  if(conf.enableBackRefs) {
    DataTriggerService.registerDataTrigger('sys.internal.processBackRefs', null, 'after', true, true, true, processBackRefs, -90);
  }



  return Q(true);
  
};



/****************
 ** Ref Repair **
 ****************/
const processSingleRef = function(className, badRefs, refTd, fieldVal, arrIndex) {

  const bo = this;
  
  var refId = fieldVal._id;
  var refClass = refTd.ref_class || fieldVal.ref_class; //ref_class can be defined in the field for non-specific references
  if(!db[refClass]) {
      logger.warn('found reference to nonexistant class %s -> %s', className, refClass);
      var badRef = {
        bo:bo, field:refTd.field_name, refId:refId, arrIndex:arrIndex
      };
      //Clean these up later!
      badRefs.push(badRef);
      return Q(true);
  }
  logger.silly(`processSingleRef: ${className} -> ${refClass}.${refId}`);

  //Check if the reference is valid, if so update disp and denormalized fields
  return db[refClass].count({_id:refId}).exec().then(function(matchCount) {
    //If reference is valid, make the entry:
    if(matchCount === 1) {
      //Valid reference!
      var ir = new IncomingRefModel({
        target_id:refId,
        target_class: refClass,
        referencing_class: className,
        referencing_id: bo._id,
        referencing_field: refTd.field_name,
        referenced_from_array: !!refTd.is_array
      });
      logger.silly("%s.%s.%s -> %s.%s", className, ir.referencing_id, ir.referencing_field, refClass, refId);
      return ir.save().then(function() {
        return db[refClass].findById(refId).then(
          augmentRef.bind(bo[refTd.field_name], bo, refTd.field_name, refTd)
        )
        .then(function() {
          return bo.save({useVersionId:bo.__ver, skipTriggers:true}, null);
        });
      });
    }
    else {
      var badRef = {
        bo:bo, field:refTd.field_name, refId:refId, arrIndex:arrIndex
      };
      logger.warn('found bad reference %s -> %s\n%j', className, refClass, badRef);

      //Clean these up later!
      badRefs.push(badRef);
    }
  });
};

const repairRefsForBod = async function(bod) {

  var deferred = Q.defer();
  var badRefs = [];


  var className = bod.class_name;
  var typeDescMap = bod.definition;

  logger.verbose('Building IncomingRefs for %s', className);
  
  //Pull out the reference fields for this BO class
  var myRefFields = []; //an array of td's for this BOD's reference fields, augmented w/ field_name and is_array
  for(var f in typeDescMap) {
    var td = typeDescMap[f];
    if(!td) continue;
    
    logger.debug('... checking type descriptor: %j', td);
    
    if(td.type === 'reference' ||
      (Array.isArray(td) && td[0].type === 'reference')) {
      if(Array.isArray(td)) {
        td = td[0];
        td.is_array = true;
      }
      td.field_name = f;
      myRefFields.push(td);
    }
  }

  logger.debug('myRefFields %j', myRefFields);

  //if this class has reference fields...
  if(myRefFields.length > 0) {
      
    var currentPromise = Q(true); //will be processing objects one at a time; this holds promise resolved when most recent object completes
      
    var projection = {__ver:1};
    for(var i=0; i < myRefFields.length; i++) {
      projection[myRefFields[i].field_name] = 1;
    }

    //Stream in all the objects of this type, grabbing the ref fields:
    logger.debug(`Streaming all ${bod.class_name} objects...`);
    // var objStream = db[bod.class_name].find({}, projection).stream();

    var promChain = Q(true);

    for await (const bo of db[className].find({}, projection)) {
      logger.silly(`processing refs in ${bod.class_name}.${bo._id}`);
      
      _.forEach(myRefFields, function(refTd) {
        var fieldVal = bo[refTd.field_name];
        if(!fieldVal || !fieldVal._id) return;

        if(!refTd.is_array) {
          // refHandlePromises.push(processSingleRef(className, badRefs, refTd, fieldVal));
          promChain = promChain.then(processSingleRef.bind(bo, className, badRefs, refTd, fieldVal));
        }
        else {
          for(var i=0; i < fieldVal.length; i++) {
            if(fieldVal[i] && fieldVal[i]._id) {
              // refHandlePromises.push(processSingleRef(className, badRefs, refTd, fieldVal[i], i));
              promChain = promChain.then(processSingleRef.bind(bo, className, badRefs, refTd, fieldVal[i], i));
            }
          }
        }
      });//end ref field iteration
      
    }

    return promChain.then(()=>badRefs);
  }
};

/**
 * Traverse all BusinessObjects in the system and catalog all references
 * Rebuild IncomingRefModel table and
 * update all reference fields so that _disp and denormalized fields are up-to-date
 */
exports.repair = function() {
  logger.info('PERFORMING REF REPAIR...');

  if(!IncomingRefModel) {
    initRefModel();
  }

  return IncomingRefModel.remove({}).then(function() {
    logger.verbose('...IncomingRefs deleted');
    return db.BusinessObjectDef.find({}).exec();
  })
  .then(function(bodList) {

    var bodPromiseChain = Q(true);
    _.forEach(bodList, function(bod) {
      bodPromiseChain = bodPromiseChain.then(()=>repairRefsForBod(bod), err=>logger.error(err));
    }); //end _.forEach()


    // return Q.allSettled(bodPromiseList);
    return bodPromiseChain;
  })
  .then(function(bodPromiseResults) {
    logger.debug('completed processing all BODs');
    var promiseList = [];
    _.forEach(bodPromiseResults, function(promiseResult) {
      if(promiseResult.state === 'fulfilled' && Array.isArray(promiseResult.value) && promiseResult.value.length > 0) {

        _.forEach(promiseResult.value, function(badRef) {
          var bo = badRef.bo;
          var field = badRef.field;
          var arrIndex = badRef.arrIndex;
          if(arrIndex !== undefined) {
            bo[field][arrIndex] = null;
          }
          else {
            bo[field] = null;
          }
          promiseList.push(bo.save({useVersionId:bo.__ver, skipTriggers:true}, null));
        });
      }
    });
    return Q.all(promiseList);
  })
  ;
};



