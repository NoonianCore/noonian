/*
Copyright (C) 2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const Q = require('q');
const _ = require('lodash');
const semver = require('semver');
const request = require('request');

const path = require('path');
const fs = require('fs');
const spawn = require('child_process').spawn;

const db = require('../index');



const logger = require('../../logger').get('sys.pkg.pkg_manager');

const serverConf = require('../../../conf');

const nodeExecPath = process.execPath;  //full path of node executable
const nodeBinPath = path.dirname(nodeExecPath); //directory containing the node executable

const instanceHome = serverConf.instanceDir;
const noonianHome = process.env.NOONIAN_HOME;

/**
 * Execute command (in node bin dir) w/ args; 
 * @return promise resolving to object: {stdout:'..',stderr:'..',exitCode:#,success:boolean}
*/
const executeCommand = function(cmd, args, cwd) {

	cwd = cwd || instanceHome;

	const deferred = Q.defer();

	const resultObj = {
		cmd, args,
    	stdout:'',
    	stderr:''
    };

    const handleError = err=>{
    	logger.error('ERROR SPAWNING COMMAND "%s" %j  -> %j', cmd, args, resultObj).error(err);
        deferred.reject(err);
    };

	try {
		const spawnOpts = {
			cwd,
			env: { 
				PATH:process.env.PATH
			},
			stdio: ['ignore','pipe','pipe']
		};

        const subProc = spawn(cmd, args, spawnOpts);
        
        logger.info('executing "%s" \n using args: %j', cmd, args).debug('%j', spawnOpts);

        subProc.stdout.on('data', (data) => {
        	resultObj.stdout += data;
        });
        subProc.stderr.on('data', (data) => {
        	resultObj.stderr += data;
        });
        subProc.on('close', (code) => {            
        	resultObj.exitCode = code;
        	resultObj.success = (code===0);
        	deferred.resolve(resultObj);
        });
        
        subProc.on('error', handleError);
    }
    catch(err) {
    	handleError(err);        
    }
    
    return deferred.promise;

};

/**
 * execute command that returns json to stdout
 * @return promise resolving to parsed json
 */
const executeJsonCommand = function(cmd, args, cwd) {
	return executeCommand(cmd, args, cwd).then(result =>{
		logger.debug('... result: %j', result);

		if(result.stdout) {
			try{
        		return JSON.parse(result.stdout);
        	}
	        catch(err) {
	            throw new Error('bad json returned by npm: '+result.stdout);
	        }
        }
        else {
        	var err = 'Invalid stdout from '+cmd;
        	logger.error('%s \n %j', err, result);
        	throw new Error(err);
        }
        
	});
}


/**
* member function of both npm and bower; 
* retrieve package tree and it cache for later
*/
const getPackageTree = function() {
	if(this.pkg_tree) {
		return Q(this.pkg_tree);
	}

	const executable = path.resolve(nodeBinPath, this.executable);

	var newPkgTree = {};

	return executeJsonCommand(executable, this.get_tree_args_local, instanceHome).then(
			instanceResult => {
				// this.pkg_tree = result;
				_.assign(newPkgTree, instanceResult.dependencies);
				if(this.get_tree_args_global) {
					return executeJsonCommand(executable, this.get_tree_args_global, noonianHome);
				}
				return null;
			}
		)
		.then(
			noonHomeResult=> {
				if(noonHomeResult) {
					_.assign(newPkgTree, noonHomeResult.dependencies);
				}
				this.pkg_tree = newPkgTree;
				return newPkgTree;
			}
		)
		;
}

const npm = {
	executable:'npm',
	get_tree_args_local:['ls', '--json'],
	get_tree_args_global:['ls', '-g', '--json'],
	getPackageTree,

	/**
	 * @return object mapping NPM package name to installed version
	*/
	getInstalledVersions: function(pkgNames) {
		return this.getPackageTree().then(deps => {
			

			if(!pkgNames) {
				pkgNames = Object.keys(deps);
			}

			const result = {};

			_.forEach(pkgNames, pkg => {
				if(deps[pkg]) {
					result[pkg] = deps[pkg].version;
				}
			});

			return result;
		});
	},

	installPackage: function(name, version) {
		var pkgSpec = version ? name+'@'+version : name;
		logger.info('installing %s', pkgSpec);
		this.pkg_tree = null;
		return executeCommand(nodeBinPath+'/npm', ['install', pkgSpec]);
	}


};




const bower = {
	executable:'bower',
	get_tree_args_local:['list', '--offline', '--json'],
	getPackageTree,

	confPath:path.resolve(instanceHome, 'bower.json'),

	readConf: function() {
		var bowerJson;

		try {
			fs.statSync(this.confPath);
			bowerJson = JSON.parse(fs.readFileSync(this.confPath, 'UTF-8'));
		} catch(err) {
			bowerJson = {
				name:serverConf.instanceName,
				version:'0.0.0'
			};
		}

		if(!bowerJson.dependencies) {
			bowerJson.dependencies = {};
		}

		return bowerJson;
	},

	writeConf: function(contents) {
		fs.writeFileSync(this.confPath, JSON.stringify(contents, null, 3));
	},

	/**
	 * @return object mapping bower package name to installed version
	*/
	getInstalledVersions: function(pkgNames) {
		return this.getPackageTree().then(deps => {

			if(!pkgNames) {
				pkgNames = Object.keys(deps);
			}

			const result = {};

			_.forEach(pkgNames, pkg => {
				if(deps[pkg]) {
					result[pkg] = deps[pkg].pkgMeta.version;
				}
			});

			return result;
		});
	},

	installPackage: function(name, version) {
		//Add it to instance directory's bower.json and run bower install
		var bowerJson = this.readConf();

		if(!bowerJson.dependencies[name]) {
			bowerJson.dependencies[name] = version  || '*';

			this.writeConf(bowerJson);
			return executeCommand(nodeBinPath+'/bower', ['install', '-F']);
		}
		else {
			logger.info('Bower package %s dependency already exists:%s (asking to install %s)', name, bowerJson.dependencies[name], version);
			return {message:'skipping install of package '+name};
		}
	}
};


const noonian = {
	/*
	  Backward compatible with BusinessObjectPackage.major_version and minor_version fields
	*/
	getPkgVersion: function(bop) {
		var v = bop.version || ((bop.major_version||'0')+'.'+(bop.minor_version||'0')+'.0');
		return new semver(semver.coerce(v));
	},

	getInstalledVersions: function(pkgKeys) {
		const query = {};
		if(pkgKeys && pkgKeys.length) {
			query.key = {$in:pkgKeys};
		}

		return db.BusinessObjectPackage.find(query,{key:1,version:1,major_version:1,minor_version:1}).then(bopList => {
			const result = {};

			_.forEach(bopList, bop => {
				result[bop.key] = this.getPkgVersion(bop).toString();
			});

			return result;
		});
	},

	/*
	  Go through RemotePackageRepositories, query for 
	*/
	getMetaData: function(pkgKeys) {
		logger.debug('getMetaData for %j', pkgKeys);
		if(!pkgKeys || (pkgKeys instanceof Array && !pkgKeys.length) || !Object.keys(pkgKeys).length) {
			return Q({});
		}


		//Inner function that makes the http call to a remote repo, asking for 
		const callRemoteRepo = function(rpr) {
			const deferred = Q.defer();

			if(!rpr || !rpr.url || !rpr.auth || !rpr.auth.token) {
				deferred.reject('Bad RemotePackageRepository entry');
			}

			const keyList = encodeURIComponent(JSON.stringify(pkgKeys));
			const fullUrl = `${rpr.url}/ws/package_repo/getMetaData?keys=${keyList}`;
			
			request(
				{
					method:'GET',
					uri:fullUrl,
					auth:{bearer:rpr.auth.token},
					json:true
				},
				function(err, httpResponse, body) {
					if(err || body.error) {
						logger.debug('bad response from remote noonian repo\n%s', body && body.error || err).debug(err);
						return deferred.reject(err || body && body.error);
					}

					deferred.resolve({
						repo:rpr,
						metadata:body  // pkgkey->metadata obj
					});
				}
			);

			return deferred.promise;
		};

		
		return db.RemotePackageRepository.find({}).then(rprList=>{
			const promiseList = [];
			_.forEach(rprList, rpr=>{
				promiseList.push(callRemoteRepo(rpr));
			});

			return Q.allSettled(promiseList).then(function(promises) {
				var result = [];
				_.forEach(promises, p=>{
					if(p.state === 'fulfilled') {
						result.push(p.value);
					}
				});
				return result;
			})
			;
		});
	},

	/**
	* call RemotePackageRepository to get specified pacakge/version
	*  @return readablestream of json from server 
	*/
	getPackage:function(key, version, repo) {

		var fullUrl = `${repo.url}/ws/package_repo/getPackage?key=${key}`;

		if(version) {
			fullUrl += `&version=${version.toString()}`;
		}

		const requestParams = {
            uri:fullUrl,
            rejectUnauthorized: false
        };

        if(repo.auth) {
        	requestParams.auth = {bearer:repo.auth.token};
        }
        
        return request.get(requestParams);
	}
};

module.exports = {npm, bower, noonian};