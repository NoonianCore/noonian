
module.exports = {
	//If repo requires authentication, repository value should be an object with "url" and "auth" keys
	repository:'https://pkg.noonian.org', 
	bootstrap_packages:[
		'core',
		'app.dbui2'
	]
};