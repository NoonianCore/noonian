/**
 * Express configuration
 */

'use strict';
const path = require('path');
const fs = require('fs');

const conf = require('./index');

const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const errorHandler = require('errorhandler');

const passport = require('passport');

module.exports = function(app) {

  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');

  app.use(compression());

  app.use(/.*_raw_postbody.*/, bodyParser.text({type: '*/x-www-form-urlencoded'})); 
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json({limit:(conf.maxRequestBodySize || '10mb')}));

  app.use(methodOverride());
  app.use(cookieParser());
  app.use(passport.initialize());

  //Check if instance directory has a directory for static content
  //TODO: future version - all static content served from instance directory
  // (somehow support sharing static content between instances, e.g. via symlink)

  var staticContent = conf.staticContent || [];

  if(!(staticContent instanceof Array)) {
    staticContent = [staticContent];
  }

  staticContent.push('client');

  for(let i=0; i < staticContent.length; i++) {
    let dir = path.resolve(conf.instanceDir, staticContent[i]);

    try {
      fs.statSync(dir);
      console.log('Using directory for static content: %s', dir);
      if(conf.urlBase) {
        app.use(conf.urlBase, express.static(dir));
      }
      else {
        app.use(express.static(dir));
      }

    } catch(err) {
      if(i < staticContent.length-1) {
        console.error('NOT FOUND: Configured static content directory %s', dir);
      }
    }

  }

  if(conf.dev) {
    app.use(morgan('dev'));
    app.use(errorHandler()); // Error handler - has to be last
  }

};
