_noonian() 
{
    local cur prev instances instcommands allcommands
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    instances=`cat ~/.noonian/instance-list`
    instcommands="start stop restart bootstrap remove dbdump dbshell open watch"
    allcommands="start stop restart list status init bootstrap add remove dbdump dbshell open watch pm2-eco "
    

    if [ "$prev" = "noonian" ] ; then
    	COMPREPLY=( $(compgen -W "${allcommands}" -- ${cur}) )
    	return 0;
    fi

    COMPREPLY=( $(compgen -W "${instances}" -- ${cur}) )
    return 0;
}
complete -F _noonian noonian
