DBUI Basics
===========

Noonian's graphical user interface is referred to as **DBUI**: an initialism for "DataBase User Interface".  

The DBUI is what you see when you log in, and provides all of the screens for viewing and editing Business Objects in the system.  It provides a rich interface for managing your data, and is designed to serve as a starting point for your database application.  

This section will guide you through the key concepts around the DBUI, and how to customize and extend it to build your own application.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tour
   configuration
   query-builder
   perspectives
   menus
   custom-pages
   ui-actions

.. for screenshots, put browser window in 1024x768