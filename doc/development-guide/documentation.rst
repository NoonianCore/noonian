Documentation
=============

Most of the Business Objects you create in the process of building an application have a **Documentation** field you can use to include relevant documentation for the respective components.  These fields support JSDoc and reStructuredText, and can be used to generate API and development documenation for your Noonian applications.

TODO

.. todo::
   pending implementation of Noonian Doc Generation



..todo:: 
   Application Diagrams add-on
