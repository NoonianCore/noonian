Building a Non-DBUI Angular Application
=======================================

The guide thus far has show how it is possible to build a full-featured application by extending and customizing the DBUI.  This section describes how you can use Noonian to build an Angular.js application that is not based on DBUI.  In this scenario, it is still possible to leverage the DBUI and Noonian's data layer and APIs for your application.

A practical example of a use case might be a lightweight Angular-based front-end that leverages Noonian for the data layer and business logic, and the DBUI as an administrative back-end.

**Note: A basic understanding of Angular.js compoenents is assumed for this section**


Angular Components
------------------

