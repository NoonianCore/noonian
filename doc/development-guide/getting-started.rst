Getting Started
===============

This section will guide you through the process of installing Noonian and its dependencies, and setting up a fresh instance.

These instructions should generally apply to Linux, Windows, and MacOS hosts.


Installation
------------

The dependencies to required by Noonian are: 

1. `Node.js <https://nodejs.org/en/download/>`__ 
2. `MongoDB <https://www.mongodb.com/download-center?jmp=nav#community>`__
3. `bower <https://bower.io>`__


Mongo DB
~~~~~~~~

Follow the instructions in the `MongoDB documentation <https://docs.mongodb.com/manual/installation/?jmp=footer&_ga=2.23881914.1626274738.1558626728-547197231.1550946579#mongodb-community-edition-installation-tutorials>`__ to get MongoDB installed on your system.

Ideally, you will be able to perform a system-wide installation and run
it as a service. However, if you do not have root access to the machine
on which you are running, it is possible to run it from a directory
under user home directory.


Node.js
~~~~~~~

Package managers provide the easiest way to get the latest version
installed on your system. The nodejs website provides a `comprehensive
list of available packages <https://nodejs.org/en/download/package-manager/>`__ for most operating system hosts.

The Node.js installation includes the Node Package Manger `npm <https://npmjs.com>`, which
will be used for the rest of the installation process.


Bower
~~~~~

At the commandline, install bower globally using npm:

.. code:: console

    npm install -g bower

*If you performed a system-wide node install, you'll need to perform the above command as the root or administrative user.*


Noonian
~~~~~~~

At the commandline, install noonian globally with npm:

.. code:: console

     sudo npm install -g noonian

*NOTE: if you are installing to a system-wide node install (as opposed
to a node installation that is owned by a non-root user), there may be
issues installing dependencies compiled via node-gyp. To get around the
issue, use the "unsafe-perm" parameter:*

.. code:: console

    npm install --unsafe-perm -g noonian


*This will be resovled in a future version that will have refactored out
any dependencies that are not pure javascript.*



Instance Setup
--------------

First, create a directory for your instance, and make it your current
working directory.

.. code:: console

    mkdir my-noonian-instance
    cd my-noonian-instance

Then use the `Noonian CLI <noonian-cli>`__ to initialize the directory
as a noonian instance:

.. code:: console

    noonian init my-instance

This will create a new file **instance-config.js**, which may be edited
to `configure the instance <instance-configuration>`__.

Edit the configuration file, and then use the CLI to bootstrap the
database for the instance:

.. code:: console

    noonian bootstrap -p adminPassword

This will initialize the configured database with the core system,
setting the initial password for admin to "adminPassword".

Use the CLI to start the instance:

.. code:: console

    noonian start

This will start the instance as a background service (forked process),
and direct stdout and stderr to stdout.log and stderr.log.

You may stop it with the CLI as well:

.. code:: console

    noonian stop my-instance

(if your current working directory is the instance directory, the
"my-instance" can be omitted)


