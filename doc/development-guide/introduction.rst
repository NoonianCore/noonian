Introduction
============



What is it?
-----------

-  It is a MEAN-based integrated platform for rapid development of
   full-featured browser-based applications.
-  It is a framework for defining data objects for persistence, and for
   creating business logic around those objects.
-  It is its own browser-based development environment for full-stack
   javascript applications.

What can I do with it?
----------------------

-  Define your data objects using meaningful field types.
-  Edit those objects within a rich, customizable UI that allows you to
   build a complete CRUD interface with a few lines of JSON.
-  Build your application's UI entirely based on that built-in UI by
   adding buttons, triggers, web services, user roles and permissions,
   and custom pages.
-  AND/OR build your application's UI to be completely distinct,
   leveraging Noonian to organize the Angular components, and for its
   data APIs and web services.

