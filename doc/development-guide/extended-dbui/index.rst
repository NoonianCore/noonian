Extended DBUI Features
======================

This section takes you through several useful DBUI features not yet mentioned in this guide. 


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pivot-page
   reporting
   i18n

