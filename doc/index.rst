Welcome to Noonian Development Documentation
=============================================

This documentation is divided into two parts:

Development Guide
   
   A guide meant to be read in sequence to learn how to develop the system.

Reference
   
   The nitty-gritty technical details you'll probably need to refer back to while developing on the system.

.. todo::
   User Guide: general DBUI usage for non-developer end-users of applications built from the DBUI.  Showing how to use features such as the query editor, Perspective/column editor, CSV export, ...


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   development-guide/index
   reference/index
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
