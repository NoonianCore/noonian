Noonian Documentation
=====================

See [noonian.readthedocs.io](https://noonian.readthedocs.io/) for Noonian documentation.

This directory contains the [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) source for Noonian Documentation, and can be built using [Sphinx](https://www.sphinx-doc.org/en/master/usage/quickstart.html).

This project is integrated with [readthedocs.org](https://readthedocs.org/), which automatically builds and deploys the documentation in this directory.
