Command Line Interface
======================


::

    Usage: noonian [options] [command]

    Options:
      -V, --version                       output the version number
      -h, --help                          output usage information

    Commands:
      start [options] [instanceName]      launch an instance
      stop [options] [instanceName]       stop a running instance
      restart [options] [instanceName]    restart a running instance
      env [options] [instanceName]        show environment variables for launching an instance (returns source-able list of variable export lines)
      list                                list configured instances
      status                              list the currently-running Noonian instances
      startall                            Start all of the configured Noonian instances
      init [options] <instanceName>       initialize directory for a instance
      bootstrap [options] [instanceName]  bootstrap database for an instance
      add [options]                       add an existing instance to the index
      remove <instanceName>               remove an instance from the instance index
      dbdump [options] [instanceName]     call mongodump to create a dump of the database
      dbshell [options] [instanceName]    start mongo shell for the instance's database
      open [options] [instanceName]       launch browser window
      watch [options] [instanceName]      watch stdout and stderr of an instance
      pm2-eco                             generate pm2 ecosystem JSON (print to stdout)


Instance Set-up
---------------

init, bootstrap, add, remove




Process Management
-------------------

The CLI contains basic functionality for managing Noonian instances on the local machine.  These features are meant for use in a development envrionment.  For a production deployment, either a Docker container or a more sophisticated process management tool such as PM2 is recommended.

Start / Stop / Restart

Status

PM2 Ecosystem Generation



Utilities
---------

Database Dump

Database Shell

Get Environment Variables



Installing Bash Autocompletion
------------------------------

The Noonian CLI has a bash auto-completion script so commands and
instance names can be auto-completed at the command prompt.

If you have root access to the system, you can create a symbolic link in
**/etc/bash\_completion.d** to the
NOONIAN\_HOME/template/bash\_completion.d

.. code:: bash

    ln -s /path/to/installation/node_modules/noonian/template/bash_completion.sh /etc/bash_completion.d/noonian

If you do not have root access, you can simply source the
bash\_completion.sh in your .bashrc. (Alternatively, refer to `this
discussion for other
options <https://serverfault.com/questions/506612/standard-place-for-user-defined-bash-completion-d-scripts>`__)
