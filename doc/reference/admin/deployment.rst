Deployment
==========

This document describes various options for production deployment of a Noonian instance.



PM2
----

Run Noonian as a service using `PM2 <https://pm2.keymetrics.io/>`__


Proxy through Apache
~~~~~~~~~~~~~~~~~~~~



Docker
------

There are multiple scenarios for deploying Noonian using docker:

*  All-in-one, single instance
*  All-in-one, multi-instance
*  External MongoDB, single-instance Noonian container
*  External MongoDB, multi-instance Noonian container


All-in-one Container
~~~~~~~~~~~~~~~~~~~~

Full Node + MongoDB + Noonian stack in one image/container.



Docker-Compose Stack
~~~~~~~~~~~~~~~~~~~~

Two separate containers: one MongoDB, one Node.js/Noonian 


